"""
Configuration file utility
"""

import copy
import os
from pathlib import Path
import click
from typeguard import typechecked  # see https://github.com/agronholm/typeguard
import ruamel.yaml as yaml

from sarar.decorators import specificemptychecked
import sarar.commands.check_deck as cd

SARAR_CONFIG_PATH: str = os.getenv("SARAR_CONFIG", "")
SARAR_XDG_CONFIG: str = os.getenv("XDG_CONFIG_HOME", "") + "/sarar.config"
SARAR_HOME_CONFIG: str = str(Path.home()) + "/.config" + "/sarar.config"

default_config: dict = {  # default configuration values
    "options": {
        "initial-interval": "1 day",
        "max-interval": "1 year",
        "min-interval": "1 day",
        "review-window": "* 4 * * *",
        "max-reviews-per-review-window": 100,
        "max-new-cards-per-review-window": 10,
        "review-wrong-card-until-right": True,
        "wrong-card-specific-interval": "10 minutes",
        "print-tags-on-front": "no",
        "print-tags-on-back": "yes",
        "card-answers": [
            {"name": "EASY", "new-card-interval": "interval*2"},
            {"name": "GOOD", "new-card-interval": "interval*1.2"},
            {"name": "OK", "new-card-interval": "interval*1"},
            {"name": "MEH", "new-card-interval": "interval/1.2"},
            {"name": "HARD", "new-card-interval": "interval/2"},
            {
                "name": "KO",
                "new-card-interval": "1 day",
                "is-wrong": True,
            },
        ],
    }
}
config = copy.deepcopy(default_config)


@typechecked
def get_config_path() -> str:
    """
    Retrieve the SARAR configuration file if any.

    Here are the paths, by order of priority, where the config file will be searched:
    - $SARAR_CONFIG
    - $XDG_CONFIG_HOME/sarar.config
    - $HOME/.config/sarar.config

    :returns: str: a string representation of the path to the config file (if any), or None else
    :raises Exception: click: exception thrown no configuration file has been found
    """
    config_path: str = ""
    if SARAR_CONFIG_PATH:
        config_path = SARAR_CONFIG_PATH
    elif SARAR_XDG_CONFIG:
        config_path = SARAR_XDG_CONFIG
    elif SARAR_HOME_CONFIG:
        config_path = SARAR_HOME_CONFIG
    else:
        click.secho(
            "\n"
            "No configuration file has been found! The paths where the config file will be "
            "searched, by order of priority, are: `$SARAR_CONFIG`, "
            "`$XDG_CONFIG_HOME/sarar.config` and `$HOME/.config/sarar.config`.",
            fg="red",
            err=True,
        )
        raise click.Abort()
    return config_path


def load_config():
    """
    Load the configuration file.

    :raises Exception: click: exception thrown if the config file can't be loaded
    :raises Exception: click: exception thrown if the config file can't be parsed (bad YAML syntax)
    """
    config_path: str = get_config_path()
    if config_path and os.path.exists(config_path):
        try:
            stream = open(config_path, encoding="utf-8")  # pylint: disable=consider-using-with
        except Exception as exc:
            click.secho(f"{exc}", fg="red")
            click.secho(
                f"\n"
                f"An unknown error occured when trying to load the `{config_path}` config "
                f"file. This might be due to the permissions of this config file not being "
                f"appropriate for the current user."
                f"\n",
                fg="red",
                err=True,
            )
            raise click.Abort()

        try:
            retrieved_config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            click.secho(f"{exc}\n", fg="red")
            click.secho(
                f"\n"
                f"The config file `{config_path}` cannot be parsed, because its YAML "
                f"syntax is not valid! Check it, e.g. on https://yamlvalidator.com/",
                fg="red",
                err=True,
            )
            raise click.Abort()
        else:
            if retrieved_config:
                check_config(config_path, retrieved_config)
                parse_config(retrieved_config)
            else:
                check_config(config_path, {})
                parse_config({})


@typechecked
def parse_config(retrieved_config: dict):
    """
    Parse the content of the retrieved config and overwrite the default config with it.
    """
    global config  # pylint: disable=global-statement, invalid-name
    if retrieved_config:
        for (key, val) in retrieved_config.items():
            if isinstance(val, dict):
                for subkey in val:
                    config[key][subkey] = retrieved_config[key][subkey]
            else:
                config[key] = retrieved_config[key]


config_keys = [
    "collection-path",
    "options",
]


@typechecked
@specificemptychecked("config_path")
def check_config(config_path: str, retrieved_config: dict):
    """
    Check the content of the configuration file.

    :param config_path: str: path to the .config file to check
    :param retrieved_config: dict: TODO
    """
    if not retrieved_config:
        click.secho(
            f"\n"
            f"The config file `{config_path}` is empty, the default configuration will be used.",
            fg="yellow",
        )
        # log warning here
        return

    for key in retrieved_config:
        if key not in config_keys:
            click.secho(
                f"\n"
                f"The config file `{config_path}` contains an unknown key: `{key}`!\n"
                f"Make sure your config file has a valid structure:\n"
                f"https://sarar.readthedocs.io/en/latest/user-guide/configuration/",
                fg="red",
                err=True,
            )
            raise click.Abort()

    if "options" in retrieved_config:
        try:
            # re-use the check_deck `check_options(...)` function in order to verify the content of
            # the `options` list.
            cd.check_options(config_path, options=retrieved_config["options"])
        except Exception as exc:  # pylint: disable=broad-except
            click.secho(
                f"\n"
                f"An error has been detected when checking the `options` of the config file "
                f"`{config_path}` (see the above error message).\n"
                f"The `options` structure in a config file is same than in a deck file, that's "
                f"why you should make sure your file has a valid SARAR Architecture.\n"
                f"Furthermore, you might also want to make sure that your config file has a valid "
                f"structure:\n "
                f"https://sarar.readthedocs.io/en/latest/user-guide/configuration/",
                fg="red",
            )
            raise exc
