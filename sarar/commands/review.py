"""
review command
"""

from datetime import datetime, timedelta
import readline
import click
import ruamel.yaml as yaml
from typeguard import typechecked  # see https://github.com/agronholm/typeguard

from sarar.decorators import emptychecked

import sarar.commands.check_deck as cd
import sarar.config as cfg

card_answers = []


@typechecked
@emptychecked
def review(deck_path: str, with_hints: bool):
    """
    Review the given deck file after checking it (see `sarar/commands/check_deck.py`).

    :param deck_path: str: path to the deck file to print
    """
    cd.check_deck(deck_path)
    data = yaml.safe_load(open(deck_path))

    # retrieve card answers from deck of from config:
    card_answers_str: str = ""
    i: int = 0
    for card_answer in cfg.config["options"]["card-answers"]:
        card_answers.append(f"{i}. {card_answer['name']}")
        card_answers_str += card_answer["name"] + ", "
        i += 1
    card_answers_str = card_answers_str[:-2]

    # see https://stackoverflow.com/a/2046054 and https://stackoverflow.com/a/44257929
    # readline.parse_and_bind("tab: complete")
    # readline.parse_and_bind("tab: menu-complete : complete")
    readline.parse_and_bind("tab: menu-complete")
    readline.set_completer(complete)

    # review
    for card in data["cards"]:
        if is_card_due_for_review(card):
            review_card(card, card_answers_str, with_hints)


@typechecked
@emptychecked
def is_card_due_for_review(card: dict) -> bool:
    """
    ...
    """
    if "last-review" in card:
        # "last-review" date is present: the card should be reviewed only if the "last-review"
        # date plus the "interval" duration is less or equal to now

        last_review = datetime.strptime(card["last-review"], "%Y/%m/%d %H:%M:%S %z")
        if "interval" in card:
            interval = card["interval"]
        else:
            interval = cfg.config["initial-interval"]

        interval_seconds: int = cd.aeval(cd.parse_and_replace_expression(interval, None))
        next_review = last_review + timedelta(seconds=interval_seconds)

        # see https://stackoverflow.com/a/39079819
        local_timezone = datetime.now().astimezone().tzinfo

        if "is-wrong" in card:
            # "is-wrong" flag is present: the card should be reviewed only if the
            # "review-wrong-card-until-right" deck option is true and if the "last-review" date plus
            # the "wrong-card-specific-interval" duration is less or equal to now

            if cfg.config["options"]["review-wrong-card-until-right"]:

                wrong_card_specific_interval: str = cfg.config["options"][
                    "wrong-card-specific-interval"
                ]
                expr: str = cd.parse_and_replace_expression(wrong_card_specific_interval, None)
                wrong_card_specific_interval_seconds: int = cd.aeval(expr)
                next_review = last_review + timedelta(seconds=wrong_card_specific_interval_seconds)

                # see https://stackoverflow.com/a/39079819
                local_timezone = datetime.now().astimezone().tzinfo

                return datetime.now(tz=local_timezone) > next_review
            else:
                return False
        else:
            return datetime.now(tz=local_timezone) > next_review

    else:
        # no "last-review" date: the card can be reviewed immediately
        return True


@typechecked
@emptychecked
def review_card(card: dict, card_answers_str: str, with_hints: bool):
    """
    ...
    """

    click.clear()
    if "last-review" not in card:
        if with_hints:
            click.secho(
                "Hint: This is a new card (it has never been reviewed)!\n", dim=True, italic=True
            )

    if "interval" not in card:
        card["interval"] = cfg.config["options"]["initial-interval"]

    click.secho("Front:\n", underline=True)
    click.echo(f"{card['front']}")
    # click.confirm("")
    if "back" in card:

        if with_hints:
            click.secho(
                "\nHint: Press any key to see the back of the card ...", dim=True, italic=True
            )

        click.pause("\n")
        click.secho("Back:\n", underline=True)
        click.echo(f"{card['back']}\n")
        click.echo(f"Enter one of the following card-answers: {card_answers_str} ")
        if with_hints:
            click.secho(
                "Hint: You can use <Tab> to cycle through card-answers and for completion.",
                dim=True,
                italic=True,
            )
        answer = input()
        while answer not in card_answers:
            answer = input("Bad input, please enter a valid card answer: ")
        current_card_interval = cd.aeval(cd.parse_and_replace_expression(card["interval"], None))
        new_card_interval = get_new_card_interval(answer, current_card_interval)

        if with_hints:
            click.secho(
                f"Hint: New interval to wait before next review: "
                f"`{get_nice_interval(new_card_interval)}` (old one: `{card['interval']}`).",
                dim=True,
                italic=True,
            )
            click.secho(
                "\nHint: Press any key to see the front of the next card ...", dim=True, italic=True
            )

        click.pause("\n")
    else:
        click.secho("\nHint: Press any key to see the next card ...", dim=True, italic=True)
        click.pause("\n")


@typechecked
@emptychecked
def get_new_card_interval(answer: str, current_card_interval: int) -> int:
    """
    ...
    """
    new_card_interval: int = 0
    for card_answer in cfg.config["options"]["card-answers"]:
        if card_answer["name"] == answer:
            expr = cd.parse_and_replace_expression(
                card_answer["new-card-interval"], current_card_interval
            )
            new_card_interval = int(cd.aeval(expr))
            break

    return new_card_interval


@typechecked
def get_nice_interval(interval: int) -> str:  # pylint: disable=too-many-statements
    """
    ...
    """
    nice_interval: str = ""

    years: int = 0
    months: int = 0
    weeks: int = 0
    weeks: int = 0
    days: int = 0
    hours: int = 0
    minutes: int = 0
    seconds: int = 0

    while True:
        if interval >= 60 * 60 * 24 * 365:
            years += 1
            interval -= 60 * 60 * 24 * 365
            continue
        if interval >= 60 * 60 * 24 * 30:
            months += 1
            interval -= 60 * 60 * 24 * 30
            continue
        if interval >= 60 * 60 * 24 * 7:
            weeks += 1
            interval -= 60 * 60 * 24 * 7
            continue
        if interval >= 60 * 60 * 24:
            days += 1
            interval -= 60 * 60 * 24
            continue
        if interval >= 60 * 60:
            hours += 1
            interval -= 60 * 60
            continue
        if interval >= 60:
            minutes += 1
            interval -= 60
            continue
        if interval >= 1:
            seconds += 1
            interval -= 1
            continue
        break

    if years > 1:
        nice_interval += f"{years} years "
    elif years == 1:
        nice_interval += f"{years} year "

    if months > 1:
        nice_interval += f"{months} months "
    elif months == 1:
        nice_interval += f"{months} month "

    if weeks > 1:
        nice_interval += f"{weeks} weeks "
    elif weeks == 1:
        nice_interval += f"{weeks} week "

    if days > 1:
        nice_interval += f"{days} days "
    elif days == 1:
        nice_interval += f"{days} day "

    if hours > 1:
        nice_interval += f"{hours} hours "
    elif hours == 1:
        nice_interval += f"{hours} hour "

    if minutes > 1:
        nice_interval += f"{minutes} minutes "
    elif minutes == 1:
        nice_interval += f"{minutes} minute "

    if seconds > 1:
        nice_interval += f"{seconds} seconds"
    elif seconds == 1:
        nice_interval += f"{seconds} second"

    return nice_interval.rstrip()


def complete(text, state):
    """
    see https://stackoverflow.com/a/2046054 and https://stackoverflow.com/a/44257929
    """
    cmd_to_return: str
    for cmd in card_answers:
        if cmd.startswith(text):
            if state:
                state -= 1
            else:
                cmd_to_return = cmd
                break
    return cmd_to_return
