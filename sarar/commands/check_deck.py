"""
check-deck command
"""

from datetime import datetime
from asteval import Interpreter
import click
import ruamel.yaml as yaml
from typeguard import typechecked  # see https://github.com/agronholm/typeguard
import pycron
from sarar.decorators import emptychecked, specificemptychecked

aeval = Interpreter()

mandatory_deck_items = [
    "name",
    "cards",
]
options_keys = [
    "initial-interval",
    "max-interval",
    "min-interval",
    "review-window",
    "max-reviews-per-review-window",
    "max-new-cards-per-review-window",
    "review-wrong-card-until-right",
    "wrong-card-specific-interval",
    "print-tags-on-front",
    "print-tags-on-back",
    "card-answers",
]
card_keys = [
    "front",
    "back",
    "tags",
    "last-review",
    "interval",
    "is-wrong",
]
mandatory_card_keys = [
    "front",
]
card_answer_list_members = [
    "name",
    "new-card-interval",
    "is-wrong",
]
mandatory_card_answer_keys = [
    "name",
    "new-card-interval",
]

boolean_keys = [
    "print-tags-on-front",
    "print-tags-on-back",
    "review-wrong-card-until-right",
    "is-wrong",
]
integer_keys = [
    "max-reviews-per-review-window",
    "max-new-cards-per-review-window",
]
string_keys = [
    "name",
    "front",
    "back",
]
date_keys = [
    "last-review",
]
duration_keys = [
    "initial-interval",
    "max-interval",
    "min-interval",
    "wrong-card-specific-interval",
    "new-card-interval",
    "interval",
]
cron_keys = [
    "review-window",
]

replacers = {
    "initial-interval": "1",  #
    "max-interval": "1",  #
    "min-interval": "1",  #
    "years": "*60*60*24*365",
    "year": "*60*60*24*365",
    "months": "*60*60*24*30",
    "month": "*60*60*24*30",
    "weeks": "*60*60*24*7",
    "week": "*60*60*24*7",
    "days": "*60*60*24",
    "day": "*60*60*24",
    "hours": "*60*60",
    "hour": "*60*60",
    "minutes": "*60",
    "minute": "*60",
    "seconds": "*1",
    "second": "*1",
}

accepted_booleans = [
    True,
    "True",
    "true",
    "TRUE",
    1,
    "1",
    "Yes",
    "yes",
    "YES",
    "y",
    "Y",
    False,
    "False",
    "false",
    "FALSE",
    0,
    "0",
    "No",
    "no",
    "NO",
    "n",
    "N",
]


@typechecked
@emptychecked
def check_deck(file_path: str):
    """
    Checks if the given file has a valid YAML syntax and has a valid SARAR Architecture.

    - See https://yaml.org/ for more details about the YAML syntax (YAML 1.2 should be supported).

    - See https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture/ for more details
      about the SARAR Architecture.

    :param file_path: str: path to the file to check
    :raises exception: click: thrown if the YAML or SARAR syntax of the file is not valid
    """

    try:
        stream = open(file_path)  # pylint: disable=consider-using-with
    except Exception as exc:
        click.secho(f"{exc}", fg="red")
        click.secho(
            f"\n"
            f"An unknown error occured when trying to load the `{file_path}` deck file. "
            f"This might be due to the permissions of this config file not being "
            f"appropriate for the current user.",
            fg="red",
            err=True,
        )
        raise click.Abort()

    # parse YAML content in order to retrieve its data
    try:
        data = yaml.safe_load(stream)

        # check presence of mandatory items in a deck:
        check_mandatory_items_in_deck(file_path, data)

        # check `name`, `deck` and `cards` content, and also check unknown item(s)
        for item in data:
            if item == "name":
                pass
            elif item == "options":
                check_options(file_path, options=data["options"])
            elif item == "cards":
                check_cards(file_path, cards=data["cards"])
            else:
                click.secho(
                    f"\n"
                    f"The file `{file_path}` contains an unknown `{item}` item!\n"
                    f"Make sure your file has a valid SARAR Architecture:\n"
                    f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
                    fg="red",
                    err=True,
                )
                raise click.Abort()

    except yaml.YAMLError as exc:
        click.secho(f"{exc}", fg="red")
        click.secho(
            f"\n"
            f"The file `{file_path}` cannot be parsed, because its YAML syntax "
            f"is not valid! Check it, e.g. on https://yamlvalidator.com/",
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@emptychecked
def check_mandatory_items_in_deck(file_path: str, data: dict):
    """
    Checks the presence of the mandatory item(s) in a deck.

    :param file_path: str: path to the file being checked
    :param data: dict: top level YAML keys/values (items) of the file
    :raises exception: click: thrown if a mandatory item isn't present
    """
    for item in mandatory_deck_items:
        if item not in data:
            click.secho(
                f"\n"
                f"The file `{file_path}` does not contains the mandatory `{item}` item. "
                f"Make sure your file has a valid SARAR Architecture:\n"
                f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@specificemptychecked("file_path")
def check_options(file_path: str, options: dict):
    """
    Checks the content of the `options` dictionnary.

    :param file_path: str: path to the file being checked
    :param options: dict: dictionnary containing options keys/values
    :raises exception: click: thrown if options dict is empty
    """
    if options:
        check_unknown_keys_in_options(file_path, options)
        check_known_keys_in_options(file_path, options)
    else:
        click.secho(
            f"\n"
            f"The file `{file_path}` should not contains emtpy `options`!\n"
            f"Make sure your file has a valid SARAR Architecture:\n"
            f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@emptychecked
def check_unknown_keys_in_options(file_path: str, options: dict):
    """
    Checks if any unknown key is in the `options` dictionnary.

    :param file_path: str: path to the file being checked
    :param options: dict: dictionnary containing options keys/values
    :raises exception: click: thrown if an unknown key is in the options dictionnary
    """
    for key in options:
        if key not in options_keys:
            click.secho(
                f"\n"
                f"The file `{file_path}` contains an unknown `{key}` key in the "
                f"`options` dictionnary.\n"
                f"Make sure your file has a valid SARAR Architecture:\n"
                f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@emptychecked
def check_known_keys_in_options(file_path: str, options: dict):
    """
    Checks the content of the known keys of the `options` dictionnary.

    :param file_path: str: path to the file being checked
    :param options: dict: dictionnary containing options keys/values
    :raises exception: click: thrown if a field is not handled properly by sarar
    """
    for key in options:
        if key in boolean_keys:
            check_boolean_field(file_path, key, options[key])
        elif key in integer_keys:
            check_integer_field(file_path, key, options[key])
        elif key in duration_keys:
            check_duration_field(file_path, key, options[key])
        elif key in cron_keys:
            check_cron_field(file_path, key, options[key])
        elif key == "card-answers":
            check_card_answers_option(file_path, options[key])
        else:
            click.secho(
                f"\n"
                f"The file `{file_path}` contains a field `{key}` that is not handled "
                f"properly by sarar! Please create an issue here: "
                f"https://gitlab.com/stephane.tzvetkov/sarar/-/issues with this error message, in "
                f"order for sarar to be corrected asap.",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@emptychecked
def check_card_answers_option(file_path: str, card_answers: list):
    """
    Checks the content of the `card-answers` option.

    :param file_path: str: path to the file being checked
    :param card_answers: list: list of card-answer in the `card-answers` option
    :raises exception: click: thrown if a field is not handled properly by sarar
    """
    for card_answer in card_answers:
        check_unknown_key_presence_in_card_answer(file_path, card_answer)
        check_mandatory_keys_presence_in_card_answer(file_path, card_answer)
        for field_key in card_answer:
            if field_key in boolean_keys:
                check_boolean_field(file_path, field_key, card_answer[field_key])
            elif field_key in string_keys:
                check_string_field(file_path, field_key, card_answer[field_key])
            elif field_key in duration_keys:
                check_duration_field(file_path, field_key, card_answer[field_key])
            else:
                click.secho(
                    f"\n"
                    f"The file `{file_path}` contains the card-answer "
                    f"`{card_answer['name']}` (in the card-answers option) with the field "
                    f"`{field_key}` which is not handled properly by sarar! Please create an "
                    f"issue here: https://gitlab.com/stephane.tzvetkov/sarar/-/issues with this "
                    f"error message, in order for sarar to be corrected asap.",
                    fg="red",
                    err=True,
                )
                raise click.Abort()


@typechecked
@emptychecked
def check_unknown_key_presence_in_card_answer(file_path: str, card_answer: dict):
    """
    Checks the presence of unknown key in a card-answer dictionnary.

    :param file_path: str: path to the file being checked
    :param card_answer: dict: dictionnary containing card-answer keys/values
    :raises exception: click: thrown if an unknown key is found in the card-answer dictionnary
    """
    for card_answer_key in card_answer:
        if card_answer_key not in card_answer_list_members:
            click.secho(
                f"\n"
                f"The file `{file_path}` contains an unknown `{card_answer_key}` key"
                f"in a `card-answer` sub-dictionnary of the `options` dictionnary.\n"
                f"Make sure your file has a valid SARAR Architecture:\n"
                f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@emptychecked
def check_mandatory_keys_presence_in_card_answer(file_path: str, card_answer: dict):
    """
    Checks the mandatory presence of the some key(s) in a card-answer dictionnary.

    :param file_path: str: path to the file being checked
    :param card_answer: dict: dictionnary containing card-answer keys/values
    :raises exception: click: thrown if the `new-card-interval` key isn't in the card-answer dict
    """
    for mandatory_card_answer_key in mandatory_card_answer_keys:
        if mandatory_card_answer_key not in card_answer:
            click.secho(
                f"\n"
                f"The file `{file_path}` does not contains the mandatory "
                f"`{mandatory_card_answer_key}` key in the `card-answer` sub-dictionnary of the "
                f"`options` dictionnary.\n"
                f"Make sure your file has a valid SARAR Architecture:\n"
                f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@emptychecked
def check_cards(file_path: str, cards: list):
    """
    Checks the content of the cards list.

    :param file_path: str: path to the file being checked
    :param cards: list: cards of the deck
    """
    for card in cards:
        check_unknown_keys_in_card(file_path, card)
        check_mandatory_keys_presence_in_card(file_path, card)
        check_known_keys_in_card(file_path, card)


@typechecked
@emptychecked
def check_unknown_keys_in_card(file_path: str, card: dict):
    """
    Checks if an unknown key is in the card.

    :param file_path: str: path to the file being checked
    :param card: dict: a card of the deck
    :raises exception: click: thrown if an unknown key is in the deck
    """
    for key in card:
        if key not in card_keys:
            click.secho(
                f"\n"
                f"The file `{file_path}` contains an unknown `{key}` key in a `card` "
                f"dictionnary. Make sure your file has a valid SARAR Architecture:\n"
                f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@emptychecked
def check_mandatory_keys_presence_in_card(file_path: str, card: dict):
    """
    Checks the presence of the mandatory keys in a card.

    :param file_path: str: path to the file being checked
    :param card: dict: a card of the deck
    :raises exception: click: thrown if a mandatory key isn't present in the card
    """
    for mandatory_card_key in mandatory_card_keys:
        if mandatory_card_key not in card:
            click.secho(
                f"\n"
                f"The file `{file_path}` does not contains the mandatory `{mandatory_card_key}` "
                f"key in one of its cards. Make sure your file has a valid SARAR Architecture:\n"
                f"https://sarar.readthedocs.io/en/latest/user-guide/sarar-architecture",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@emptychecked
def check_known_keys_in_card(file_path: str, card: dict):
    """
    Checks the content of the known keys of a `card` dictionnary.

    :param file_path: str: path to the file being checked
    :param card: dict: dictionnary containing card's keys/values
    :raises exception: click: thrown if a field is not handled properly by sarar
    """
    for card_key in card.keys():
        if card_key in boolean_keys:
            check_boolean_field(file_path, card_key, card[card_key])
        elif card_key in string_keys:
            check_string_field(file_path, card_key, card[card_key])
        elif card_key in date_keys:
            check_date_field(file_path, card_key, card[card_key])
        elif card_key in duration_keys:
            check_duration_field(file_path, card_key, card[card_key])
        elif card_key == "tags":
            for tag in card["tags"]:
                check_string_field(file_path, card_key, tag)
        else:
            click.secho(
                f"\n"
                f"The file `{file_path}` contains a field `{card_key}` that is not handled "
                f"properly by sarar. Please create an issue here: "
                f"https://gitlab.com/stephane.tzvetkov/sarar/-/issues with this error message, in "
                f"order for sarar to be corrected asap.",
                fg="red",
                err=True,
            )
            raise click.Abort()


@typechecked
@emptychecked
def check_boolean_field(file_path: str, field_key: str, field_value):
    """
    Checks the value of a field, which should be a boolean.

    :param file_path: str: path to the file being checked
    :param field_key: str: the field key
    :param field_value: str: the field value
    :raises exception: click: thrown if the `field_value` is not valid
    """
    if field_value not in accepted_booleans:
        click.secho(
            f"\n"
            f"The file `{file_path}` contains a bad value associated to the `{field_key}` field. "
            f"The value `{field_value}` of this field could not be parsed and converted into a "
            f"boolean. Accepted values for booleans are 'True', 'true', 'TRUE', '1', 'False', "
            f"'FALSE', '0'.\n",
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@emptychecked
def check_integer_field(file_path: str, field_key: str, field_value):
    """
    Checks the value of a field, which should be an integer.

    :param file_path: str: path to the file being checked
    :param field_key: str: the field key
    :param field_value: str: the field value
    :raises exception: click: thrown if the `field_value` is not valid
    """
    if not isinstance(field_value, int):
        click.secho(
            f"\n"
            f"The file `{file_path}` contains a bad value associated to the `{field_key}` field. "
            f"The value `{field_value}` of this field could not be parsed and converted into a "
            f"integer.\n",
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@emptychecked
def check_string_field(file_path: str, field_key: str, field_value):
    """
    Checks the value of a field, which should be a string.

    :param file_path: str: path to the file being checked
    :param field_key: str: the field key
    :param field_value: str: the field value
    :raises exception: click: thrown if the `field_value` is not valid
    """
    try:
        field_value.isprintable()
    except Exception as exc:
        click.secho(f"{exc}", fg="red")
        click.secho(
            f"\n"
            f"The file `{file_path}` contains a bad value associated to the `{field_key}` field. "
            f"The value `{field_value}` of this field could not be parsed and converted into a "
            f"string.\n",
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@emptychecked
def check_date_field(file_path: str, field_key: str, field_value: str):
    """
    Checks the value of a field, which should be a date.

    :param file_path: str: path to the file being checked
    :param field_key: str: the field key
    :param field_value: str: the field value
    :raises exception: click: thrown if the `field_value` is not valid
    """
    try:
        datetime.strptime(field_value, "%Y/%m/%d %H:%M:%S %z")
    except Exception as exc:
        click.secho(f"{exc}", fg="red")
        click.secho(
            f"\n"
            f"The file `{file_path}` contains a bad value associated to the `{field_key}` field. "
            f"The value `{field_value}` of this field could not be parsed and converted into a "
            f'date. The expected date format is "year/month/day hour:minute:second timezone", '
            f'e.g. "2000/01/01 01:01:01 +0000".\n',
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@emptychecked
def check_duration_field(file_path: str, field_key: str, field_value: str):
    """
    Checks the value of a field, which should a valid duration.

    :param file_path: str: path to the file being checked
    :param field_key: str: the field key
    :param field_value: str: the field value
    :raises exception: click: thrown if the `field_value` is not valid
    """
    expr = parse_and_replace_expression(field_value, 1)  # replace interval by 1 just for the check
    aeval(expr)
    if len(aeval.error) > 0:
        errmsg = ""
        for err in aeval.error:
            errmsg = f"{errmsg}\n{err.get_error()}"
        errmsg = f"{errmsg}\n"
        click.secho(
            f"\n"
            f"The file `{file_path}` contains a bad value associated to the `{field_key}` field. "
            f"The value `{field_value}` of this field could not be parsed and converted into a "
            f"duration because of the below error:\n"
            f"{errmsg}",
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@emptychecked
def check_cron_field(file_path: str, field_key: str, field_value):
    """
    Checks the value of a field, which should a valid cron expression.

    :param file_path: str: path to the file being checked
    :param field_key: str: the field key
    :param field_value: str: the field value
    :raises exception: click: thrown if the `field_value` is not valid
    """
    try:
        pycron.is_now(field_value)
    except Exception as exc:
        click.secho(f"{exc}", fg="red")
        click.secho(
            f"\n"
            f"The file `{file_path}` contains a bad value associated to the `{field_key}` field. "
            f"The value `{field_value}` of this field could not be parsed and converted into a "
            f"cron expression.\n",
            fg="red",
            err=True,
        )
        raise click.Abort()


@typechecked
@specificemptychecked("expr")
def parse_and_replace_expression(expr: str, interval: [int, None]) -> str:
    """
    Parses a string, mathematical expression representing a duration, and replaces a set of
    key words by their associated number of seconds.

    :param expr: str: the expression to parse
    :param interval: int: the interval in seconds
    :returns: str: the parsed expression with key words replaced
    """
    for rep in replacers:
        if rep in expr:
            expr = expr.replace(rep, replacers[rep])
    if "interval" in expr:
        if interval:
            expr = expr.replace("interval", str(interval))
        else:
            click.secho(
                f"\nAn interval needs to be replaced in the following expression: `{expr}`, but "
                f"none has been provided!\n",
                fg="red",
                err=True,
            )
            raise click.Abort()

    return expr
