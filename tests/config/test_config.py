"""
Tests configuration mechanisms.
"""

import copy
import os
from pathlib import Path
import pytest
import click

import sarar.config as cfg  # pylint: disable=import-error


def test_default_config():
    """
    Test the default config values.
    """

    cfg.config = copy.deepcopy(cfg.default_config)
    default_config_from_python_file = copy.deepcopy(cfg.default_config)

    cfg.SARAR_CONFIG_PATH = "./tests/config/valid_default_config.config"
    cfg.load_config()
    default_config_from_config_file = copy.deepcopy(cfg.config)
    assert default_config_from_python_file == default_config_from_config_file

    cfg.SARAR_CONFIG_PATH = "./tests/config/valid_detailed_config.config"
    cfg.load_config()
    valid_config_from_config_file = copy.deepcopy(cfg.config)
    assert default_config_from_python_file != valid_config_from_config_file

    cfg.SARAR_CONFIG_PATH = "./tests/config/valid_minimal_config.config"
    cfg.load_config()
    minimal_config_from_config_file = copy.deepcopy(cfg.config)
    assert valid_config_from_config_file == minimal_config_from_config_file


def test_get_config_path():
    """
    Test how the config file is retrieved (if any).
    """

    test_config_path = "/sarar/config/path/to/config/file.config"
    cfg.SARAR_CONFIG_PATH = test_config_path
    config_path: Path = Path(cfg.get_config_path())
    assert config_path == Path(test_config_path)
    cfg.SARAR_CONFIG_PATH = None

    test_config_path = "/sarar/xdg/config/path/to/config/file.config"
    cfg.SARAR_XDG_CONFIG = test_config_path
    config_path: Path = Path(cfg.get_config_path())
    assert config_path == Path(test_config_path)
    cfg.SARAR_XDG_CONFIG = None

    test_config_path = "/sarar/home/config/path/to/config/file.config"
    cfg.SARAR_HOME_CONFIG = test_config_path
    config_path: Path = Path(cfg.get_config_path())
    assert config_path == Path(test_config_path)
    cfg.SARAR_HOME_CONFIG = None

    with pytest.raises(click.Abort):
        config_path: Path = Path(cfg.get_config_path())


def test_load_config():
    """
    Test how the config file is checked after being loaded.
    """

    cfg.SARAR_CONFIG_PATH = "./tests/config/valid_detailed_config.config"
    cfg.load_config()

    cfg.SARAR_CONFIG_PATH = "/path/to/nowhere"
    cfg.load_config()

    cfg.SARAR_CONFIG_PATH = "./tests/config/invalid_empty_config.config"
    cfg.load_config()

    invalid_permissions_config = "./tests/config/invalid_permissions_config.config"
    os.chmod(invalid_permissions_config, 0o000)
    with pytest.raises(click.Abort):
        cfg.SARAR_CONFIG_PATH = invalid_permissions_config
        cfg.load_config()
    os.chmod(invalid_permissions_config, 0o644)

    with pytest.raises(click.Abort):
        cfg.SARAR_CONFIG_PATH = "./tests/config/invalid_syntax_config.config"
        cfg.load_config()


valid_minimal_config = {
    "options": {
        "initial-interval": "2 days",
    },
}
invalid_minimal_config = {
    "unknown": "foo",
}


def test_parse_config():
    """
    Test how the retrieved config file is parsed and how it overwrite the default config.
    """

    cfg.config = copy.deepcopy(cfg.default_config)

    cfg.parse_config({"options": {"initial-interval": "42 days"}})
    assert cfg.config["options"]["initial-interval"] == "42 days"

    cfg.config["options"]["initial-interval"] = "1 day"
    assert cfg.config == cfg.default_config

    cfg.parse_config({})
    assert cfg.config == cfg.default_config


def test_check_config():
    """
    Test how the config file is checked after being loaded.
    """

    cfg.check_config("/path/to/foo-config", valid_minimal_config)

    with pytest.raises(click.Abort):
        cfg.check_config("/path/to/foo-config", invalid_minimal_config)

    with pytest.raises(click.Abort):
        cfg.check_config("/path/to/foo-config", {"options": {}})

    with pytest.raises(Exception):
        cfg.check_config("", valid_minimal_config)
