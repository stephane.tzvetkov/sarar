"""
Tests file associated to the `check-deck` command.
"""

from pathlib import Path
import click
from click.testing import CliRunner
import pytest

import sarar.commands.check_deck as cd  # pylint: disable=import-error
from sarar.sarar import check_deck  # pylint: disable=no-name-in-module, wrong-import-order
from sarar.sarar import main  # pylint: disable=no-name-in-module, wrong-import-order

valid_minimal_deck_items = {
    "name": "foo name",
    "cards": [],
}

invalid_minimal_deck_items = {
    "unknown": "foo",
}


def test_check_deck_command():
    """
    Test directly the `check-deck` command in order to make sure that the test deck of reference is
    valid and that flawed decks raise exceptions.
    """
    runner = CliRunner()

    result = runner.invoke(main, ["-c/path/to/foo", "check-deck", "-h"])
    # print(result.output)
    assert result.exit_code == 0

    path = Path("./tests/decks/valid_detailed_deck.deck").absolute()
    result = runner.invoke(check_deck, [f"-d{path}"])
    assert result.exit_code == 0

    result = runner.invoke(check_deck, ["-h"])
    assert result.exit_code == 0

    path = Path("./tests/decks/invalid_syntax_deck.deck").absolute()
    result = runner.invoke(check_deck, [f"-d{path}"])
    assert result.exit_code == 1

    path = Path("./tests/decks/invalid_detailed_deck.deck").absolute()
    result = runner.invoke(check_deck, [f"-d{path}"])
    assert result.exit_code == 1

    path = Path("./tests/decks/valid_detailed_deck.deck").absolute()
    result = runner.invoke(check_deck, [f"-d{path} -h"])
    assert result.exit_code == 1

    path = Path("./tests/decks/valid_detailed_deck.deck").absolute()
    result = runner.invoke(main, ["-c/path/to/foo", "check-deck", f"-d{path}", "-h"])
    assert result.exit_code == 2


def test_check_mandatory_items_in_deck():
    """
    Test `check_mandatory_items_in_deck(...)` in order to make sure that a deck contains the
    mandatory items, and that an exception is raised if not.
    """
    cd.check_mandatory_items_in_deck(file_path="/path/to/foo-deck", data=valid_minimal_deck_items)

    with pytest.raises(click.Abort):
        cd.check_mandatory_items_in_deck(
            file_path="/path/to/foo-deck", data=invalid_minimal_deck_items
        )


valid_detailed_options = {
    "initial-interval": "2 day",
    "max-interval": "2 year",
    "min-interval": "2 day",
    "review-window": "* 2 * * *",
    "max-reviews-per-review-window": 200,
    "max-new-cards-per-review-window": 20,
    "review-wrong-card-until-right": True,
    "wrong-card-specific-interval": "30 minutes",
    "print-tags-on-front": "no",
    "print-tags-on-back": "yes",
    "card-answers": [
        {"name": "OK", "new-card-interval": "interval"},
        {"name": "HARD", "new-card-interval": "interval/2", "is-wrong": False},
        {"name": "KO", "new-card-interval": "1 day", "is-wrong": True},
    ],
}
invalid_minimal_options = {
    "unknown": "foo",
}


def test_check_options():
    """
    Test `check_options(...)` in order to make sure that the option dictionnary does
    not contains any unknown key , and that an exception is raised else.
    """
    cd.check_options(file_path="/path/to/foo-deck", options=valid_detailed_options)

    with pytest.raises(click.Abort):
        cd.check_options(file_path="/path/to/foo-deck", options=invalid_minimal_options)


def test_check_unknown_keys_in_options():
    """
    Test `check_unknown_keys_in_options(...)` in order to make sure that the option dictionnary
    does not contains any unknown key, and that an exception is raised else.
    """
    cd.check_unknown_keys_in_options(file_path="/path/to/foo-deck", options=valid_detailed_options)

    with pytest.raises(click.Abort):
        cd.check_unknown_keys_in_options(
            file_path="/path/to/foo-deck", options=invalid_minimal_options
        )


def test_check_known_keys_in_options():
    """
    Test `check_known_keys_in_options(...)` in order to make sure that the option dictionnary
    contains valid key-value pairs, and that an exception is raised if not.
    """
    cd.check_known_keys_in_options(file_path="/path/to/foo-deck", options=valid_detailed_options)

    with pytest.raises(click.Abort):
        cd.check_known_keys_in_options(
            file_path="/path/to/foo-deck", options=invalid_minimal_options
        )


valid_detailed_card_answer = {"name": "FOO", "new-card-interval": "interval/2", "is-wrong": False}
invalid_detailed_card_answer = {"name": "FOO", "new-card-interval": "1", "unknown": "foo"}
valid_minimal_card_answer = {"name": "FOO", "new-card-interval": "interval/2"}
invalid_minimal_card_answer = {"name": "FOO"}


def test_check_card_answers_option():
    """
    Test `check_card_answers_option(...)` in order to make sure that each card-answer in a list are
    valid, and that an exception is raised if not.
    """
    valid_card_answers = [valid_detailed_card_answer, valid_minimal_card_answer]
    cd.check_card_answers_option(file_path="/path/to/foo-deck", card_answers=valid_card_answers)

    invalid_card_answers = valid_card_answers.append(invalid_detailed_card_answer)
    with pytest.raises(TypeError):
        cd.check_card_answers_option(
            file_path="/path/to/foo-deck", card_answers=invalid_card_answers
        )

    keys = cd.string_keys
    cd.string_keys = []
    with pytest.raises(click.Abort):
        cd.check_card_answers_option(
            file_path="/path/to/foo-deck",
            card_answers=[{"name": "plop", "new-card-interval": "interval"}],
        )
    cd.string_keys = keys


def test_check_unknown_key_presence_in_card_answer():
    """
    Test `check_unknown_key_presence_in_card_answer(...)` in order to make sure of that a card
    answer does not contain any unknown key(s), and that an exception is raised else.
    """
    cd.check_unknown_key_presence_in_card_answer(
        file_path="/path/to/foo-deck", card_answer=valid_detailed_card_answer
    )
    with pytest.raises(click.Abort):
        cd.check_unknown_key_presence_in_card_answer(
            file_path="/path/to/foo-deck", card_answer=invalid_detailed_card_answer
        )


def test_check_mandatory_keys_presence_in_card_answer():
    """
    Test `check_mandatory_keys_presence_in_card_answer(...)` in order to make sure of that the
    mandatory key(s) of a card answer are present, and that an exception is raised if not.
    """
    cd.check_mandatory_keys_presence_in_card_answer(
        file_path="/path/to/foo-deck", card_answer=valid_minimal_card_answer
    )
    with pytest.raises(click.Abort):
        cd.check_mandatory_keys_presence_in_card_answer(
            file_path="/path/to/foo-deck", card_answer=invalid_minimal_card_answer
        )


valid_detailed_card = {
    "front": "foo front",
    "back": "foo back",
    "tags": ["tag1", "tag2"],
    "last-review": "2000/01/01 01:01:01 +0000",
    "interval": "42 days",
}
invalid_detailed_card = {
    "front": "foo front",
    "back": "foo back",
    "tags": ["tag1", "tag2"],
    "last-review": "2000/01/01 01:01:01 +0000",
    "interval": "42 days",
    "unknown": "foo",
}

valid_minimal_card = {
    "front": "foo front",
}
invalid_minimal_card = {
    "back": "foo back",
}


def test_check_cards():
    """
    Test `check_cards(...)` in order to make sure that each card in a list are valid, and that an
    exception is raised if not.
    """
    valid_cards = [valid_detailed_card, valid_minimal_card]
    cd.check_cards(file_path="/path/to/foo-deck", cards=valid_cards)

    invalid_cards = valid_cards.append(invalid_detailed_card)
    with pytest.raises(TypeError):
        cd.check_cards(file_path="/path/to/foo-deck", cards=invalid_cards)


def test_check_unknown_keys_in_card():
    """
    Test `check_unknown_keys_in_card(...)` in order to make sure that a card does not contain any
    unknown key(s), and that an exception is raised else.
    """
    cd.check_unknown_keys_in_card(file_path="/path/to/foo-deck", card=valid_detailed_card)

    with pytest.raises(click.Abort):
        cd.check_unknown_keys_in_card(file_path="/path/to/foo-deck", card=invalid_detailed_card)


def test_check_mandatory_keys_presence_in_card():
    """
    Test `check_mandatory_keys_presence_in_card(...)` in order to make sure that a card has the
    mandatory key(s) required, and that an exception is raised if not.
    """
    cd.check_mandatory_keys_presence_in_card(file_path="/path/to/foo-deck", card=valid_minimal_card)
    with pytest.raises(click.Abort):
        cd.check_mandatory_keys_presence_in_card(
            file_path="/path/to/foo-deck", card=invalid_minimal_card
        )


def test_check_known_keys_in_card():
    """
    Test `check_known_keys_in_card(...)` in order to make sure of the keys-values validity of a
    card, and that an exception is raised if not.
    """
    cd.check_known_keys_in_card(file_path="/path/to/foo-deck", card=valid_detailed_card)
    with pytest.raises(click.Abort):
        cd.check_known_keys_in_card(file_path="/path/to/foo-deck", card=invalid_detailed_card)


def test_check_boolean_field():
    """
    Test `check_boolean_field(...)` in order to make sure that the checked field value can be
    interpreted as a boolean, and that an exception is raised if not.
    """
    for accepted_boolean in cd.accepted_booleans:
        cd.check_boolean_field(
            file_path="/path/to/foo-deck", field_key="foo-key", field_value=accepted_boolean
        )
    with pytest.raises(click.Abort):
        cd.check_boolean_field(
            file_path="/path/to/foo-deck", field_key="foo-key", field_value="invalid"
        )


def test_check_integer_field():
    """
    Test `check_integer_field(...)` in order to make sure that the checked field value can be
    interpreted as an integer, and that an exception is raised if not.
    """
    cd.check_integer_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value=42)
    with pytest.raises(click.Abort):
        cd.check_integer_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value=1.1)
    with pytest.raises(click.Abort):
        cd.check_integer_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value="42")
    with pytest.raises(Exception):
        cd.check_integer_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value=[])


def test_check_string_field():
    """
    Test `check_string_field(...)` in order to make sure that the checked field value can be
    interpreted as a string, and that an exception is raised if not.
    """
    cd.check_string_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value="test")
    with pytest.raises(Exception):
        cd.check_string_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value=None)
    with pytest.raises(Exception):
        cd.check_string_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value="")
    with pytest.raises(click.Abort):
        cd.check_string_field(
            file_path="/path/to/foo-deck", field_key="foo-key", field_value=Exception
        )


def test_check_date_field():
    """
    Test `check_date_field(...)` in order to make sure that valid string expressions can be
    interpreted as a date, and that invalid expressions will raise an exception.
    """
    cd.check_date_field(
        file_path="/path/to/foo-deck", field_key="foo-key", field_value="2021/04/03 16:09:26 +0000"
    )
    with pytest.raises(click.Abort):
        cd.check_date_field(
            file_path="/path/to/foo-deck",
            field_key="foo-key",
            field_value="2021-04-03T16:09:26+00:00",
        )
    with pytest.raises(click.Abort):
        cd.check_date_field(
            file_path="/path/to/foo-deck",
            field_key="foo-key",
            field_value="2021-04-03T16:09:26Z",
        )
    with pytest.raises(click.Abort):
        cd.check_date_field(
            file_path="/path/to/foo-deck",
            field_key="foo-key",
            field_value="20210403T160926Z",
        )


def test_check_duration_field():
    """
    Test `check_duration_field(...)` in order to make sure that valid string expressions can be
    interpreted as a duration, and that invalid expressions will raise an exception.
    """

    cd.check_duration_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value="1")
    cd.check_duration_field(
        file_path="/path/to/foo-deck", field_key="foo-key", field_value="1 + 42"
    )

    # Allowed Python’s basic builtins functions (see
    # https://asteval.readthedocs.io/en/latest/basics.html#built-in-functions):
    #
    # abs, all, any, bin, bool, bytearray, bytes, chr, complex, dict, dir, divmod, enumerate,
    # filter, float, format, frozenset, hash, hex, id, int, isinstance, len, list, map, max, min,
    # oct, ord, pow, range, repr, reversed, round, set, slice, sorted, str, sum, tuple, type, zip
    cd.check_duration_field(
        file_path="/path/to/foo-deck", field_key="foo-key", field_value="abs(-42)"
    )

    # Allowed Python's named exceptions (see
    # https://asteval.readthedocs.io/en/latest/basics.html#built-in-functions):
    #
    # ArithmeticError, AssertionError, AttributeError, BaseException, BufferError, BytesWarning,
    # DeprecationWarning, EOFError, EnvironmentError, Exception, False, FloatingPointError,
    # GeneratorExit, IOError, ImportError, ImportWarning, IndentationError, IndexError, KeyError,
    # KeyboardInterrupt, LookupError, MemoryError, NameError, None, NotImplemented,
    # NotImplementedError, OSError, OverflowError, ReferenceError, RuntimeError, RuntimeWarning,
    # StopIteration, SyntaxError, SyntaxWarning, SystemError, SystemExit, True, TypeError,
    # UnboundLocalError, UnicodeDecodeError, UnicodeEncodeError, UnicodeError,
    # UnicodeTranslateError, UnicodeWarning, ValueError, Warning, ZeroDivisionError
    cd.check_duration_field(
        file_path="/path/to/foo-deck", field_key="foo-key", field_value="ArithmeticError"
    )

    # Allowed Python's math fonctions (see
    # https://asteval.readthedocs.io/en/latest/basics.html#built-in-functions
    #
    # acos, acosh, asin, asinh, atan, atan2, atanh, ceil, copysign, cos, cosh, degrees, e, exp,
    # fabs, factorial, floor, fmod, frexp, fsum, hypot, isinf, isnan, ldexp, log, log10, log1p,
    # modf, pi, pow, radians, sin, sinh, sqrt, tan, tanh, trunc
    cd.check_duration_field(
        file_path="/path/to/foo-deck", field_key="foo-key", field_value="acos(1)"
    )

    with pytest.raises(click.Abort):
        cd.check_duration_field(
            file_path="/path/to/deck", field_key="foo-key", field_value="unknown duration"
        )

    with pytest.raises(click.Abort):
        # Banned Python reserved key-words that cannot be in an asteval expression (see
        # https://asteval.readthedocs.io/en/latest/basics.html#accessing-the-symbol-table):
        #
        # and, as, assert, break, class, continue, def, del, elif, else, except, exec, finally,
        # for, from, global, if, import, in, is, lambda, not, or, pass, print, raise, return, try,
        # while, with, True, False, None, eval, execfile, __import__, __package__
        cd.check_duration_field(
            file_path="/path/to/foo-deck", field_key="foo-key", field_value="eval"
        )


def test_check_cron_field():
    """
    Test `check_cron_field(...)` in order to make sure that a string field can be interpreted as a
    valid cron expression, and that invalid expressions will raise an exception.
    """
    cd.check_cron_field(
        file_path="/path/to/foo-deck", field_key="foo-key", field_value="* 12 * * *"
    )
    with pytest.raises(click.Abort):
        cd.check_cron_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value=1.1)
    with pytest.raises(Exception):
        cd.check_cron_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value="")
    with pytest.raises(Exception):
        cd.check_cron_field(file_path="/path/to/foo-deck", field_key="foo-key", field_value=[])
    with pytest.raises(Exception):
        cd.check_cron_field("", "foo-key", "* 12 * * *")


def test_parse_and_replace_expression():
    """
    Test `parse_and_replace_expression(...)` in order to make sure some specific key words in the
    string expressions are well replaced, this way the expression can be interpreted as a duration.
    """
    assert cd.parse_and_replace_expression("42+initial-interval") == "42+1"
    assert cd.parse_and_replace_expression("42+max-interval") == "42+1"
    assert cd.parse_and_replace_expression("42+min-interval") == "42+1"
    assert cd.parse_and_replace_expression("42+interval") == "42+1"
    assert cd.parse_and_replace_expression("42 years") == "42 *60*60*24*365"
    assert cd.parse_and_replace_expression("1 year") == "1 *60*60*24*365"
    assert cd.parse_and_replace_expression("42 months") == "42 *60*60*24*30"
    assert cd.parse_and_replace_expression("1 month") == "1 *60*60*24*30"
    assert cd.parse_and_replace_expression("42 weeks") == "42 *60*60*24*7"
    assert cd.parse_and_replace_expression("1 week") == "1 *60*60*24*7"
    assert cd.parse_and_replace_expression("42 days") == "42 *60*60*24"
    assert cd.parse_and_replace_expression("1 days") == "1 *60*60*24"
    assert cd.parse_and_replace_expression("42 hours") == "42 *60*60"
    assert cd.parse_and_replace_expression("1 hour") == "1 *60*60"
    assert cd.parse_and_replace_expression("42 minutes") == "42 *60"
    assert cd.parse_and_replace_expression("1 minute") == "1 *60"
    assert cd.parse_and_replace_expression("42 seconds") == "42 *1"
    assert cd.parse_and_replace_expression("1 second") == "1 *1"

    for constant in cd.replacers:
        assert cd.parse_and_replace_expression(constant) == cd.replacers[constant]
