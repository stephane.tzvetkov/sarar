"""
Tests file associated to the `print-deck` command.
"""

from pathlib import Path
from click.testing import CliRunner
import pytest

import sarar.commands.print_deck as pd  # pylint: disable=import-error
from sarar.sarar import print_deck  # pylint: disable=no-name-in-module, wrong-import-order


def test_print_deck_command():
    """
    Test directly the `print-deck` command in order to make sure that valid options are completes
    without problems and that flawed options raise exceptions.
    """
    runner = CliRunner()
    path = Path("./tests/decks/valid_detailed_deck.deck").absolute()
    result = runner.invoke(print_deck, [f"-d{path}"])
    assert result.exit_code == 0

    result = runner.invoke(print_deck, ["-h"])
    assert result.exit_code == 0

    result = runner.invoke(print_deck, [f"-d{path} -h"])
    assert result.exit_code == 1


def test_print_deck():
    """
    Test `print_deck(...)` in order to make sure that any deck with a valie YAML syntax is printed,
    and that an exception is raised if not.
    """
    pd.print_deck("./tests/decks/valid_detailed_deck.deck")
    with pytest.raises(Exception):
        pd.print_deck("")
    with pytest.raises(Exception):
        pd.print_deck("/wrong/path/deck.deck")
    with pytest.raises(Exception):
        pd.print_deck("./tests/decks/invalid_syntax_deck.deck")
