# CHANGELOG

> This project follows the [semantic versioning](https://semver.org/) specification. For example,
> see how [poetry](https://python-poetry.org/docs/cli/#version) handle this specification.

## 0.1.0

* Add a command to check a deck (see [issue
  4](https://gitlab.com/stephane.tzvetkov/sarar/-/issues/4)).

* Add a command to print a deck (see [issue
  3](https://gitlab.com/stephane.tzvetkov/sarar/-/issues/3)).

* Create a very first - minimal - working version of SARAR (see [issue
  2](https://gitlab.com/stephane.tzvetkov/sarar/-/issues/2)).

* Define the initial objectives of this project, its scope, as well as its general architecture, in
  the README.md and in [the documentation
  folder](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs) (see [issue 1](
  https://gitlab.com/stephane.tzvetkov/sarar/-/issues/1)).
