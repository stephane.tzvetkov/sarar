
# Core scientific guidelines

Here are the key scientific assumptions (and associated references) that are guiding the
development of SARAR:

---

* Why practice memorizing things?
    * "*Working memory — which relates to the amount of information that can be retained in short-term working memory when engaged in processing, learning, comprehension, problem solving or goal-directed thinking. Working memory is strongly related to a person’s ability to reason with novel information (i.e., general fluid intelligence).*" (see [*Learning strategies: a synthesis and conceptual model*](#learning-strategies-a-synthesis-and-conceptual-model))

---

* Spaced and repeated learning sessions is mostly a superior technique than massed learning:
    * "*Individuals in spaced practice conditions outperformed those in massed practice conditions by almost one half of a standard deviation.*" (see [A Meta-Analytic Review of the Distribution of Practice Effect : Now You See It, Now You Don't](#a-meta-analytic-review-of-the-distribution-of-practice-effect-now-you-see-it-now-you-dont))
    * "Separating learning episodes by a period of at least 1 day, rather than concentrating all
      learning into one session, is extremely useful for maximizing long-term retention" (see
      [Distributed Practice in Verbal Recall Tasks : A Review and Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))
    * "Spaced (vs. massed) learning of items consistently shows benefits, regardless of retention
      interval, and learning benefits increase with increased time lags between learning
      presentations" (see [Distributed Practice in Verbal Recall Tasks : A Review and Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))
    * "The type of task being practiced and the amount of time given between practice sessions can
      have a profound effect on the overall superiority of spaced practice over massed practice."
      (see [A Meta-Analytic Review of the Distribution of Practice Effect : Now You See It, Now You
      Don't](#a-meta-analytic-review-of-the-distribution-of-practice-effect-now-you-see-it-now-you-dont))
    * "At this consolidation phase, the task is to review and practice (or overlearn) the material.
      Such investment is more valuable if it is spaced over time rather than massed." (see
      [*Learning strategies: a synthesis and conceptual
      model*](#learning-strategies-a-synthesis-and-conceptual-model))

  This justifies why SARAR uses the [spaced
  repetition](https://en.wikipedia.org/wiki/Spaced_repetition) technique, and why the initial
  default time interval between learning sessions is one day long.

---

* The [active recall](https://en.wikipedia.org/wiki/Active_recall) technique might be more
  efficient than passive recall, when coupled with [spaced
  repetition](https://en.wikipedia.org/wiki/Spaced_repetition):
    * "varying the spacing of the stimuli will have a larger influence when the respondent is
      actively rehearsing the material" (see [A Meta-Analysis of the Spacing Effect in Verbal
      Learning : Implications for Research on Advertising Repetition and Consumer
      Memory](#a-meta-analysis-of-the-spacing-effect-in-verbal-learning-implications-for-research-on-advertising-repetition-and-consumer-memory))

  I did not found a lot more evidences about this (help would be appreciated here). Therefore, in
  SARAR, active recall is recommended and proposed by default. But passive recall is also
  optionally available.

---

* A [flashcard](https://en.wikipedia.org/wiki/Flashcard) system is a very convenient way to
  experience spaced-repetited learning sessions with a software. I did not find any meta-analysis
  revealing the superiority or inferiority of this system compared to another one. Unless someone
  find some indisputable arguments (meta-analysis based) against it, this system will remain the
  one used in SARAR.

---

* The time interval between learning sessions becomes too long at some point:
    * "Longer spacing and/or lag intervals sometimes failed to benefit retention." (see
      [Distributed Practice in Verbal Recall Tasks : A Review and Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))
    * "It seems clear that once the interval between learning sessions reaches some relatively long
      amount of time, further increases either have no effect upon or decrease memory as measured
      in a later test" (see [Distributed Practice in Verbal Recall Tasks : A Review and
      Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))

  This justifies why a maximum interval time between sessions is configurable in SARAR (per deck of
  cards).

---

* Expanding intervals between learning sessions are not necessarily better for memorizing than
  fixed intervals:
    * "Expanding intervals either benefit learning or produce effects similar to studying with
      fixed spacing." (see [Distributed Practice in Verbal Recall Tasks : A Review and Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))
    * "After more than a century of research on spacing, much of it motivated by the obvious
      practical implications of the phenomenon, it is unfortunate that we cannot say with certainty
      how long the interstudy interval should be to optimize long-term retention." (see
      [Distributed Practice in Verbal Recall Tasks : A Review and Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))
    * "Large standard errors, indicative of large between-study variability, make conclusions drawn
      from expanding versus fixed interval data necessarily tentative." (see [Distributed Practice
      in Verbal Recall Tasks : A Review and Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))

  This justifies why the evolution of the interval time between sessions (fixed, expanding or even
  contracting) is configurable in SARAR (per deck of cards).

  However, while longer intervals are not necessarily better for memorization, they are much more
  convenient for learning more material per session. Therefore, this will be the default approach.

---

* Any non-constant [forgetting curve](https://en.wikipedia.org/wiki/Forgetting_curve) equation is
  controversial:
    * "The effects of nonconstant (i.e., expanding or contracting) learning schedules on retention
      are still poorly understood. Expanding study intervals rarely seem to produce much harm for
      recall after long delays, but there is insufficient data to say whether they help. This has
      not stopped some software developers from assuming that expanding study intervals work better
      than fixed intervals. For example, Wozniak and Gorzelanczyk (1994; see also SuperMemoWorld,
      n.d.) offered a “universal formula” designed to space repetitions ..." (see [Distributed
      Practice in Verbal Recall Tasks : A Review and Quantitative
      Synthesis](#distributed-practice-in-verbal-recall-tasks-a-review-and-quantitative-synthesis))
    * "The optimal intertrial interval appears to partially be a function of the type of task being
      learned." (see [A Meta-Analytic Review of the Distribution of Practice Effect : Now You See
      It, Now You
      Don't](#a-meta-analytic-review-of-the-distribution-of-practice-effect-now-you-see-it-now-you-dont))

  I am not aware of any meta-analysis arguing the superiority of any equation in order to
  approximate how information is lost over time when there is no attempt to retain it. Knowing how
  fast information is forgotten would be interesting in order to find when to schedule reviews.
  But, without any consensus about a model/equation describing this process, SARAR will consider
  that it depends on the individual and on the complexity/subject of the learning material.

  Therefore, the evolution of the time interval is configurable in SARAR (per deck of cards) with
  the equation you want.

---

* Some related and interesting articles:
    * https://en.wikipedia.org/wiki/Spaced_repetition
    * https://en.wikipedia.org/wiki/Flashcard
    * https://en.wikipedia.org/wiki/Active_recall
    * https://en.wikipedia.org/wiki/Forgetting_curve
    * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5126970/

If you think this section does not properly reflect the current scientific consensus, then, based
on the best **meta-analysis** you have found, let's discuss it (please open an issue) and let's
improve SARAR together.

---
## References

### Learning strategies: a synthesis and conceptual model

* See https://www.nature.com/articles/npjscilearn201613

This article from Nature (from august 2016) is a synthesis of 228 meta-analyses about learning
strategies. It addresses topics well beyond the scope of this project (not just "remembering
things"), but it really is a *must-read* if you are interested in learning strategies.

### Visible learning: A synthesis of over 800 meta-analyses relating to achievement

* See https://sci-hub.st/10.1080/01443410903415150

This book (from november 2009), as its name declares, is a synthesis of 800 meta-analyses about
learning. The scope of this book is beyond the one of this project (not just "remembering things"),
but it is a major reference if you are interested in learning.

### A Meta-Analysis of the Spacing Effect in Verbal Learning : Implications for Research on Advertising Repetition and Consumer Memory

* See https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.200.8846&rep=rep1&type=pdf

### Distributed Practice in Verbal Recall Tasks : A Review and Quantitative Synthesis

* See https://sci-hub.st/10.1037/0033-2909.132.3.354

### A Meta-Analytic Review of the Distribution of Practice Effect : Now You See It, Now You Don't

* See https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.740.6703&rep=rep1&type=pdf

