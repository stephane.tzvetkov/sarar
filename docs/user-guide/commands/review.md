# review

## NAME

review

## SYNOPSIS

sarar check-deck [-d | --deck-path] [-h | help]

## DESCRIPTION

TODO

## OPTIONS

-d, --deck-path <PATH>
    <br/>
    Path to the deck to check.


-h, --help
    <br/>
    Show this message and exit.

## EXAMPLES

Check a specific deck:
    <br/>
    `#!Bash $ sarar review -d /path/to/your/deck-file.deck`


Review all decks of your collection:
    <br/>
    `#!Bash $ sarar review`

## LICENSE

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.

This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.
