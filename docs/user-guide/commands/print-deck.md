# print-deck

## NAME

print-deck

## SYNOPSIS

sarar print-deck [-d | --deck-path] [-h | help]

## DESCRIPTION

This command will pretty print the content of the `deck` file passed with the below `-d,
--deck-path` option.

## OPTIONS

-d, --deck-path <PATH>
    <br/>
    Path to the deck to print.


-h, --help
    <br/>
    Show this message and exit.

## EXAMPLES

Print a specific deck:
    <br/>
    `#!Bash $ sarar print-deck -d /path/to/your/deck-file.deck`


## LICENSE

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.

This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.
