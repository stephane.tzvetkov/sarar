# SARARA - SARAR Architecture

## Key concepts

[Flashcards](https://en.wikipedia.org/wiki/Flashcard) related:

- **Card**: A flashcard is a card bearing information on both sides: a question (on the front side)
  and an answer (on the back side).
- **Deck**: Cards are gathered in decks. So, a deck is just a collection of cards (a deck is
  usually made up of cards sharing a common topic/theme).

[Active recall](https://en.wikipedia.org/wiki/Active_recall) related:

- **Review**: Reviewing a card consists of answering its front size.

[Spaced repetition](https://en.wikipedia.org/wiki/Spaced_repetition) related:

- **Interval**: The interval refers to the amount of time (see [how to specify an amount of
  time](#more-details-about-how-to-specify-an-amount-of-time)) to wait before the next review of a
  card.

## Syntax

A SARAR deck is just a [YAML](https://en.wikipedia.org/wiki/YAML) file, with the `.deck` file
extension. A SARAR deck file will therefore implement the latest [YAML syntax](https://yaml.org/),
with a specific set of keys/values (items). Here is a detailed example (where default `options` are
explicitly stated):

```yaml
# Comments are denoted with '#'. If used at the end of a line,
# a space should be added as a prefix ' #' (space then #).

name: deck name  # MANDATORY
options:  # a list of options that will apply to all the cards of this deck.
    initial-interval: 1 day
    max-interval: 1 year
    min-interval: 1 day
    review-window: "* 4 * * *"
    max-reviews-per-review-window: 100
    max-new-cards-per-review-window: 10
    review-wrong-card-until-right: true
    wrong-card-specific-interval: 10 minutes
    print-tags-on-front: no
    print-tags-on-back: yes
    card-answers:  # Add, remove, or modify any card-answer you want.
        - name: EASY                     # MANDATORY name of the answer
          new-card-interval: interval*2  # MANDATORY new interval after review
        - name: GOOD                       # MANDATORY
          new-card-interval: interval*1.2  # MANDATORY
        - name: OK                       # MANDATORY
          new-card-interval: interval*1  # MANDATORY
        - name: MEH                        # MANDATORY
          new-card-interval: interval/1.2  # MANDATORY
        - name: HARD                     # MANDATORY
          new-card-interval: interval/2  # MANDATORY
        - name: KO                  # MANDATORY
          new-card-interval: 1 day  # MANDATORY
          is-wrong: true
      # - name: WHATEVER                             # MANDATORY
      #   new-card-interval: 42+log(sqrt(interval))  # MANDATORY

cards:
    - front: front content of a card
             on multiple lines.      # MANDATORY
      back: back content of a card.  # The back of a card is OPTIONAL, without
                                     # it you're no longuer doing "active" but
                                     # "passive" recall (because you will just
                                     # read the front of the card).
      # items handled by SARAR:
      last-review: 2000/01/01 01:01:01 +0000  # HANDLED BY SARAR: date of
                                              # the last review
      interval: 1 day  # HANDLED BY SARAR: duration before the next review
      is-wrong: true  # HANDLED BY SARAR: indicates that the response to the
                      # last review was wrong

    - front: another front content of a card
             on multiple lines.               # MANDATORY
      back: another back content of a card.
      tags:  # Tags are OPTIONAL, just used to add some usefull info to a card
        - tag1
        - tag2
      # items handled by SARAR:
      last-review: 2000/01/01 01:01:01 +0000  # HANDLED BY SARAR: date of
                                              # the last review
      interval: 1 day  # HANDLED BY SARAR: duration before the next review

#   - ...
```

!!! note "note"
    Providing an `options` list is *not* mandatory. By default, if no `options` are specified,
    SARAR will use the default ones: explicitly printed above.

### Details

Here are more details about each possible item:

- **name**: *MANDATORY*, the name of the deck.

- **options**: *OPTIONAL*, gathers a list of options that will apply to all the cards of this deck.
  If you specify it, this list should *not* be empty.

    - **initial-interval**: *OPTIONAL*, the initial interval of the cards of this deck, before the
      first review ("1 day" by default, like explicitly stated in the [above detailed
      example](#syntax)). See [how to specify an amount of
      time](#more-details-about-how-to-specify-an-amount-of-time) for more details about how to
      tune this option.

    - **max-interval**: *OPTIONAL*, the maximum interval of the cards of this deck ("1 year" by
      default, like explicitly stated in the [above detailed example](#syntax)). See [how to
      specify an amount of time](#more-details-about-how-to-specify-an-amount-of-time) for more
      details about how to tune this option.

    - **min-interval**: *OPTIONAL*, the minimal interval of the cards of this deck ("1 day" by
      default, like explicitly stated in the [above detailed example](#syntax)). See [how to
      specify an amount of time](#more-details-about-how-to-specify-an-amount-of-time) for more
      details about how to tune this option.

    - **review-window**: *OPTIONAL*, the time window during which the number of new card to review
      won't increase. It is specified with a [cron](https://en.wikipedia.org/wiki/Cron) expression
      indicating when the time window restarts (`* 4 * * *` by default, meaning every day at 4 am,
      like explicitly stated in the [above detailed example](#syntax)). The new cards to be
      reviewed are identified at the beginning of the next review-window.

    - **max-review-per-review-window**: *OPTIONAL*, the maximum number of reviews per
      *review-window* with the current deck ("100" by default, like explicitly stated in the [above
      detailed example](#syntax)).

    - **max-new-cards-per-review-window**: *OPTIONAL*, the maximum number of new cards to be
      learned per *review-window* with the current deck ("10" by default, like explicitly stated in
      the [above detailed example](#syntax)).

    - **review-wrong-card-until-right**: *OPTIONAL*, do you want to review cards you answered
      incorrectly until you get them right *during the same review window*, with a shorter and
      dedicated interval ("true" by default, like explicitly stated in the [above detailed
      example](#syntax)).

    - **wrong-card-specific-interval**: *OPTIONAL*, the specific time interval for cards you
      answered incorrectly, it is meant to be very short ("10 minutes" by default, like explicitly
      stated in the [above detailed example](#syntax)). Note that this specific time interval is
      active only if the previous *review-wrong-card-until-right* option is true. See [how to
      specify an amount of time](#more-details-about-how-to-specify-an-amount-of-time) for more
      details about how to tune this option.

    - **print-tags-on-front**: *OPTIONAL*, should cards' tag(s) be printed on the front ("no" by
      default, like in the above detailed example).

    - **print-tags-on-back**: *OPTIONAL*, should cards' tag(s) be printed on the front ("yes" by
      default, like in the above detailed example).

    - **card-answers**: *OPTIONAL*, a list of possible answers telling how much correct or wrong
      you were when responding to a card. If you specify this option, then it will overwrite the
      default list of card answers (this default list is explicitly stated in the [above detailed
      example](#syntax)) with the ones you will specify. This option is not mandatory, but if you
      specify it: then it will be mandatory to define a least one answer. See [the below section
      "More details about `card-answers`"](#more-details-about-card-answers) for further
      information about this option.

        - **name**: *MANDATORY* the name of the answer.<br/>
          **new-card-interval**: *MANDATORY*, a mathematical expression specifying how the
          interval of the card should change after selecting this answer (e.g. `interval*2` or
          `interval/2`). See [the below section "More details about
          `new-card-interval`"](#more-details-about-new-card-interval) for more details.<br/>
          **is-wrong**: *OPTIONAL* specify if the answer should be considered wrong. In this case
          the previous options `review-wrong-card-until-right` and `wrong-card-specific-interval`
          might apply to the answered card.

!!! note "note"
    Obviously, when specifying `options` you will just overwrite the default ones
    (explicitly printed [above](#syntax)). So, e.g. when using the following simple `options`:
    ```console
    options:
        max-interval: 2 years
    ```
    The `max-interval` option will overwrite the default one (<s>`1 year`</s> → `2 years`), but
    all the other defaults options (explicitly printed [above](#syntax)) will stay the same!

- **cards**: *MANDATORY*, a list of cards to learn.

    - **front**: *MANDATORY*, content of the front card (think of it as the question that will be
      asked).

    - **back**: *OPTIONAL*, content of the back of the card (think of it as the reply of the
      question). Note that without a back you're no longuer doing "active" but rather "passive"
      recall (i.e. you will just read the front of the card).

    - **tags**: *OPTIONAL*, a list of tags, this is just used to add some usefull information to a
      card.
        - **< tag name >**

    - **last-review**: *OPTIONAL*, *HANDLED BY SARAR*, the date of the last review, this is handled
      by SARAR but you can change it if you want, just keep in mind that SARAR will eventually
      modify it. The expected date format is "year/month/day hour:minute:second timezone".

    - **interval**: *OPTIONAL*, *HANDLED BY SARAR*, the amount of time before the next review, this
      is also handled by SARAR, you can change it if you want but remember that SARAR will
      eventually modify it. See [how to specify an amount of
      time](#more-details-about-how-to-specify-an-amount-of-time) for more details about how to
      tune this option.

#### More details about `card-answers`

The `card-answers` option allow you to adjust the interval of the card you just reviewed, by
selecting an estimation of the difficulty to remember the back of the card. By default, 6 choices
are possible after reviewing a card (like explicitly stated in the [above detailed
example](#syntax)):

- **EASY**: after reading the front of the card, you remembered the back easily without hesitation,
  so its interval will be multiplied by 2.
- **GOOD**: after reading the front of the card, you remembered the back quite well, so its
  interval will be increased by 20%.
- **OK**: after reading the front of the card, you just remembered the back, so its interval will
  stay the same.
- **MEH**: after reading the front of the card, you remembered the back imperfectly and/or you took
  too long to remember it, so its interval is decreased by 20%.
- **HARD**: after reading the front of the card, you barely remembered the back and/or with great
  difficulty, so its interval is divided by 2.
- **KO**: after reading the front of the card, you didn't remembered the back, so its interval is
  reset to the **initial-interval**.

So, after a review, you will have to estimate the difficulty you had to remember the back of a card
by choosing one of the previous card answers (note that you can also specify your own card answers,
see next paragraph for more details about that). Depending on your choice, the interval of the card
will be adjusted: the interval will be increased if you think you remembered with facility (i.e.
the next review of the card will be pushed back in time), whereas the interval will be decreased if
you remembered with difficulty (i.e. the next review of the card will be brought forward in time).

Note that you can add, modify or replace those card answers the way you want, and also change their
associated `new-card-interval` options, impacting the interval of the reviewed card, e.g.:
```yaml
name: deck name
options:
    card-answers:
        - name: FIIINE
          new-card-interval: interval*4
        - name: OK
          new-card-interval: interval
        - name: ROUGH
          new-card-interval: interval/4
        - name: KO
          new-card-interval: 7 days
          is-wrong: true
        - name: BAAAD
          new-card-interval: 1 day
          is-wrong: true

cards:
    - front: foo
    # ...
```

Remember that the `card-answers` option is not mandatory, so *you don't have to* overwrite the
defaults. But if you do specify the `card-answers` option, then *you will have to* also specify at
least one card answer. And when specifying a card answer, the `name` and `new-card-interval` fields
are mandatory!

I.e. this is allowed:
```yaml
name: deck name
options:
    card-answers:
        - name: FOO
          new-card-interval: interval+42

cards:
    - front: foo
    # ...
```

But this is not allowed:
```yaml
name: deck name
options:
    card-answers:

cards:
    - front: foo
    # ...
```

#### More details about `new-card-interval`

The `new-card-interval` option is a mathematical expression parsed by
[asteval](https://newville.github.io/asteval/) (without [numpy](https://numpy.org/) extension, for
[safety reasons](https://newville.github.io/asteval/motivation.html#how-safe-is-asteval)). The
result of this expression will replace the interval of a card, after reviewing it and
selecting the associated card-answer.

This expression can use different mathematical symbols like:

- `+` (addition)
- `-` (soustraction)
- `/` (division)
- `*` (multiplication)
- `%` (modulo)
- `**` (exponential, and not `^`)
- etc.

It supports several basic Python functions: `acos`, `acosh`, `asin`, `asinh`, `atan`, `atan2`,
`atanh`, `ceil`, `copysign`, `cos`, `cosh`, `degrees`, `e`, `exp`, `fabs`, `factorial`, `floor`,
`fmod`, `frexp`, `fsum`, `hypot`, `isinf`, `isnan`, `ldexp`, `log`, `log10`, `log1p`, `modf`, `pi`,
`pow`, `radians`, `sin`, `sinh`, `sqrt`, `tan`, `tanh`, `trunc`.

The mathematical expression can use the following constants (same constants used for [specifying an
amout of time](#more-details-about-how-to-specify-an-amount-of-time)):

- `year` or `years` (interpreted as `*365*24*60*60` seconds)
- `month` or `months` (interpreted as `*30*24*60*60` seconds)
- `week` or `weeks` (interpreted as `*7*24*60*60` seconds)
- `day` or `days` (interpreted as `*24*60*60` seconds)
- `hour` or `hours` (interpreted as `*60*60` seconds)
- `minute` or `minutes` (interpreted as `*60` seconds)
- `second` or `seconds` (interpreted as `*1` second, the base unit)

It also supports the following variables:

- `interval` (the current card interval)
- `initial-interval` (the initial interval before the first review optionally overwritten in the
  deck options)
- `max-interval` (the maximum allowed interval optionally overwritten in the deck options)
- `min-interval` (the minimum allowed interval optionally overwritten in the deck options)

#### More details about how to specify an amount of time

In SARAR, the base unit of any amount of time is the second, so a simple integer can represent a
duration in seconds, e.g. `120` will be read as 120 seconds.

In addition to a raw integer, the duration expression can also contains the following constants:

- `year` or `years` (interpreted as `*365*24*60*60` seconds)
- `month` or `months` (interpreted as `*30*24*60*60` seconds)
- `week` or `weeks` (interpreted as `*7*24*60*60` seconds)
- `day` or `days` (interpreted as `*24*60*60` seconds)
- `hour` or `hours` (interpreted as `*60*60` seconds)
- `minute` or `minutes` (interpreted as `*60` seconds)
- `second` or `seconds` (interpreted as `*1` second, the base unit)

For example:

- `3 weeks + 1day-  2  hours  -3minutes+ 42 seconds` is a valid expression interpreted as `3
  *7*24*60*60 + 1 *24*60*60 - 2 *60*60 - 3 *60 + 42 *1` = `38613462` seconds.

- `1 year 2 months` is an invalid expression

## Examples

TODO
