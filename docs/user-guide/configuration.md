# Configuration

## Location

The SARAR config file will be searched, by order of priority, in the following places:

- `$SARAR_CONFIG`
- `$XDG_CONFIG_HOME/sarar.config`
- `$HOME/.config/sarar.config`

So, if you want to use a specific location for your config file (and not the default
`$XDG_CONFIG_HOME/sarar.config`, or `$HOME/.config/sarar.config` if
[XDG](https://wiki.archlinux.org/title/XDG_Base_Directory) is not set), then you just have to set
the `$SARAR_CONFIG` environment variable, e.g.:
```console
$ SARAR_CONFIG=/path/to/config sarar -v
```

!!! tip "tip"
    You can obviously set `SARAR_CONFIG` "permanently", e.g. by exporting it in your
    `$HOME/.bashrc` (or `$HOME/.zshenv`, or `$ZDOTDIR/.zshrc`, etc):
    ```console
    $ vi $HOME/.bashrc
        > ...
        > export SARAR_CONFIG=/path/to/config
    $ source $HOME/.bashrc
    ```

## Syntax

A SARAR config file is just a [YAML](https://en.wikipedia.org/wiki/YAML) file, with the `.config`
file extension. A SARAR config file will therefore implement the latest [YAML
syntax](https://yaml.org/), with a specific set of keys/values (items). Here is a detailed example
(where defaults are explicitly stated):

```yaml
# Comments are denoted with '#'. If used at the end of a line,
# a space should be added as a prefix ' #' (space then #).

# Default decks options
#######################
options:  # a list of options that will apply to all the cards of this deck.
    initial-interval: 1 day
    max-interval: 1 year
    min-interval: 1 day
    review-window: "* 4 * * *"
    max-reviews-per-review-window: 100
    max-new-cards-per-review-window: 10
    review-wrong-card-until-right: true
    wrong-card-specific-interval: 10 minutes
    print-tags-on-front: no
    print-tags-on-back: yes
    card-answers:  # Add, remove, or modify any card-answer you want.
        - name: EASY                     # MANDATORY name of the answer
          new-card-interval: interval*2  # MANDATORY new interval after review
        - name: GOOD                       # MANDATORY
          new-card-interval: interval*1.2  # MANDATORY
        - name: OK                       # MANDATORY
          new-card-interval: interval*1  # MANDATORY
        - name: MEH                        # MANDATORY
          new-card-interval: interval/1.2  # MANDATORY
        - name: HARD                     # MANDATORY
          new-card-interval: interval/2  # MANDATORY
        - name: KO                  # MANDATORY
          new-card-interval: 1 day  # MANDATORY
          is-wrong: true
      # - name: WHATEVER                             # MANDATORY
      #   new-card-interval: 42+log(sqrt(interval))  # MANDATORY
```

!!! note "note"
    Providing a config file is *not* mandatory. By default, if no configuration file has been
    found, SARAR will use the configuration explicitly printed above.

### Details

- **options**: *OPTIONAL*, gathers a list of options that will apply to all the cards of your decks
  collection. If you specify it, this list should *not* be empty. Note that this item is the same
  than the one specified in the [SARAR Architecture](./sarar-architecture.md#details), you can
  refer to it for more details.


!!! note "note"
    Obviously, when using a config file you will just overwrite the default configuration
    (explicitly printed [above](#syntax)). So, e.g. when using the following simple config file:
    ```console
    options:
        max-interval: 2 years
    ```
    The `max-interval` option will overwrite the default one (<s>`1 year`</s> → `2 years`), but
    all the other defaults options (explicitly printed [above](#syntax)) will stay the same!
