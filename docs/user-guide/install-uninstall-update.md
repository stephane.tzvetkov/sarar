# How to install, uninstall and update

## Prerequistes

TODO: find compatible Python versions

## Install

```console
$ python -m pip install sarar --user
```

## Uninstall

```console
$ python -m pip uninstall sarar --user
```

## Update

```console
$ python -m pip install --upgrade sarar --user
```
