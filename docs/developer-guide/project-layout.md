# Project layout

```console
$ tree . -a -I ".git|.pytest_cache|.venv|__pycache__|dist|htmlcov|site"
.
├── .badge_build.svg
├── .badge_coverage.svg
├── .badge_pylint.svg
├── CHANGELOG.md
├── docs
│   ├── CHANGELOG.md -> ../CHANGELOG.md
│   ├── developer-guide
│   │   ├── get-started-and-contribute.md
│   │   └── project-layout.md
│   ├── index.md -> ../README.md
│   ├── LICENSE.md -> ../LICENSE.md
│   ├── readthedocs-requirements.txt
│   ├── scientific-guidelines.md
│   └── user-guide
│       ├── basic-usage.md
│       ├── commands
│       │   ├── check-deck.md
│       │   └── print-deck.md
│       ├── configuration.md
│       ├── install-uninstall-update.md
│       └── sarar-architecture.md
├── .flake8
├── .gitignore
├── LICENSE.md
├── .mkdocs.yaml
├── poetry.lock
├── pyproject.toml
├── README.md
├── .readthedocs.yaml
├── sarar
│   ├── click_utils.py
│   ├── commands
│   │   ├── check_deck.py
│   │   └── print_deck.py
│   ├── decorators.py
│   └── sarar.py
└── tests
    ├── commands
    │   ├── test_check_deck.py
    │   └── test_print_deck.py
    ├── configs
    │   └── valid_detailed_sarar_config.conf
    ├── decks
    │   ├── invalid_detailed_deck.deck
    │   ├── invalid_syntax_deck.deck
    │   └── valid_detailed_deck.deck
    └── test_sarar.py
```

## Description

- [**.badge_build.svg**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.badge_build.svg):
  the generated build badge.
- [**.badge_coverage.svg**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.badge_coverage.svg):
  the generated coverage badge.
- [**.badge_pylint.svg**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.badge_pylint.svg):
  the generated pylint badge.
- [**CHANGELOG.md**](../CHANGELOG.md): tracks the major modifications made to the project, version
  after version.
- [**docs**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs): stores all the
  documentation of the project. Also, this folder is the root directory for
  [mkdocs](https://www.mkdocs.org/) to work, [mkdocs](https://www.mkdocs.org/) is a
  documentation-oriented static site generator (as described in [the documentation section of "how
  to get started and contribute?"](./get-started-and-contribute.md#documentation)).
    - [**CHANGELOG.md -> ../CHANGELOG.md**](../CHANGELOG.md): symlinks to the project's
      [CHANGELOG.md](../CHANGELOG.md) in order for [mkdocs](https://www.mkdocs.org/) to access it
      and serve it.
    - [**developer-guide**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs/developer-guide):
      stores the developer related documentation of the project.
        - [**get-started-and-contribute.md**](./get-started-and-contribute.md): explains for a
          developer how to enter the project and how to work on it.
        - [**project-layout.md**](./project-layout.md): the current file.
    - [**index.md -> ../README.md**](../index.md): symlinks to the project's
      [README.md](../index.md), in order for [mkdocs](https://www.mkdocs.org/) to use it as its
      index (home).
    - [**LICENSE.md -> ../LICENSE.md**](../LICENSE.md): symlinks to the project's
      [license](../LICENSE.md), in order for [mkdocs](https://www.mkdocs.org/) to access it and
      serve it.
    - [**readthedocs-requirements.txt**](../readthedocs-requirements.txt): lists the Python modules
      required in order to build the project's documentation with [mkdocs](https://www.mkdocs.org/)
      (used by [readthedocs](https://readthedocs.org/)).
    - [**scientific-guidelines.md**](../scientific-guidelines.md): explains the key scientific
      assumptions (and associated references) that are guiding the development of SARAR.
    - [**user-guide**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs/user-guide):
      stores the user related documentation of the project.
        - [**basic-usage.md**](../user-guide/basic-usage.md): explains how to use SARAR for
          newcomers.
        - [**commands**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs/user-guide/commands):
          stores the documentation describing every SARAR commands.
            - [**check-deck.md**](../user-guide/commands/check-deck.md): explains how to use the
              `check-deck` command.
            - [**print-deck.md**](../user-guide/commands/print-deck.md): explains how to use the
              `print-deck` command.
        - [**configuration.md**](../user-guide/configuration.md): explains how to use the SARAR
          configuration file.
        - [**install-uninstall-update.md**](../user-guide/install-uninstall-update.md): explains
          how to get, remove and update SARAR.
        - [**sarar-architecture.md**](../user-guide/sarar-architecture.md): explains the syntax of
          a SARAR deck file.
- [**.flake8**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.flake8):
  [flake8](https://flake8.pycqa.org/en/latest/index.html) configuration file (more details
  [here](https://flake8.pycqa.org/en/latest/user/configuration.html)).
- [**.gitignore**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.gitignore): specifies
  intentionally untracked files to ignore from the Git versionning system (see
  [here](https://git-scm.com/docs/gitignore) for more details).
- [**LICENSE.md**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/LICENSE.md): explains
  the legal framework governing the use and redistribution of SARAR.
- [**.mkdocs.yaml**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.mkdocs.yaml):
  [mkdocs](https://www.mkdocs.org/) configuration file (see
  [here](https://docs.readthedocs.io/en/latest/config-file/index.html) for more details)
- [**poetry.lock**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/poetry.lock):
  specifies the dependencies resolution of the project (see
  [here](https://python-poetry.org/docs/basic-usage/#installing-without-poetrylock) for more
  details).
- [**pyproject.toml**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/pyproject.toml):
  genirc configuration file used by - among others -
  [black](https://black.readthedocs.io/en/stable/usage_and_configuration/the_basics.html#configuration-via-a-file),
  [pylint](https://pylint.pycqa.org/en/latest/user_guide/run.html) and
  [poetry](https://python-poetry.org/docs/) (see [here](https://python-poetry.org/docs/pyproject/)
  for more details).
- [**README.md**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/README.md): the
  documentation entry point of the project.
- [**.readthedocs.yaml**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.readthedocs.yaml):
  [readthedocs](https://docs.readthedocs.io/) configuration file (see
  [here](https://docs.readthedocs.io/en/stable/config-file/v2.html) for more details)
- [**sarar**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/sarar): sarar Python module,
  stores the source code of the project.
    - [**click_utils.py**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/sarar/click_utils.py):
      some [click](https://click.palletsprojects.com/en/7.x/) custom utility classes.
    - [**commands**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/sarar/commands):
      stores the source code related to the SARAR commands.
        - [**check_deck.py**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/sarar/commands/check_deck.py):
          source code of the `check-deck` command.
        - [**print_deck.py**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/sarar/commands/print_deck.py):
          source code of `print-deck` command.
    - [**decorators.py**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/sarar/decorators.py):
      Python decorators functions for SARAR (more information about Python decorators
      [here](https://gist.github.com/Zearin/2f40b7b9cfc51132851a)).
    - [**sarar.py**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/sarar/sarar.py):
      source code entry point.
- [**tests**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/tests): stores
  [pytest](https://github.com/pytest-dev/pytest) related files and folders, as described in [the
  tests section of "how to get started and contribute?"](./get-started-and-contribute.md#tests)).
    - [**commands**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/tests/commands):
      stores source code for commands related tests.
        - [**test_check_deck**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/tests/commands/test_check_deck.py):
          source code of the tests for the `check-deck` command.
        - [**test_print_deck**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/tests/commands/test_print_deck.py):
          source code of the tests for the `print-deck` command.
    - [**configs**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/tests/configs): stores
      some SARAR `.conf` files to test different configuration cases.
        - ...
    - [**decks**](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/tests/decks): stores
      some SARAR `deck` files to test different decks and cards settings.
        - ...
    - [**test_sarar.py**](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/tests/test_sarar.py):
      main test file source code.
