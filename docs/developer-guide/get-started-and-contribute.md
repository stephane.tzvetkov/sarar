# How to get started? How to contribute?

> Remember to keep things as simple, as minimal and as usable as possible (see the [Suckless
> philosophy](https://suckless.org/philosophy/)).

---
## Code of conduct

See the [Python Code Quality Authority’s Code of
Conduct](https://meta.pycqa.org/en/latest/code-of-conduct.html).

---
## Prerequisites

### Git

The versioning system used for this project is [Git](https://git-scm.com/) (via the [GitLab Git
server](https://gitlab.com/stephane.tzvetkov/sarar)), which is a free and open source distributed
version control system.

You can install Git, and learn more about it, by following its [official
documentation](https://git-scm.com/doc).

**Note**: When committing with Git, while working on the SARAR project, *whenever possible* please
[refer to the issue you are working
on](https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html#from-commit-messages)
(e.g. add `see #42` at the end of your commit if you are working on issue number 42).

### Python 3

The programming language used to develop SARAR is [Python](https://www.python.org/).

If you are discovering Python: you can follow the [Getting Started
guide](https://www.python.org/about/gettingstarted/), in order to install it and learn more about
it.

If you are wondering what is the "Python philosophy" of doing things in this project, then I
suggest you read [this *excellent* series of
articles](https://cjolowicz.github.io/posts/hypermodern-python-01-setup/).

### Poetry

The Python package/environment manager used for this project is
[poetry](https://python-poetry.org/).

You can install poetry, and learn more about it, by following its [official
documentation]((https://python-poetry.org/docs/).

For example, here is how I installed it:

* First, define a poetry home:
```console
$ vi $HOME/.bashrc # or ~/.zshrc or ~/.zshenv or wherever
    > ...
  + >
  + > # PYTHON POETRY
  + > export POETRY_HOME="${XDG_DATA_HOME:-${HOME/.local/share}}/poetry"
  + > export PATH="$POETRY_HOME/bin:$PATH"

$ source $HOME/.bashrc # or ~/.zshrc or ~/.zshenv or wherever
```

* Then install it, without automatically modifying your path:
```console
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py > get-poetry.py
$ python get-poetry.py --no-modify-path
```

* Now, make sure poetry is up to date:
```console
$ poetry self update
$ poetry --version
```

* Optionally, you might want to create a virtual environment per project *inside* the project's
  root directory. If so, the virtual environment will be created and expected in a folder named
  `.venv`:
```console
$ poetry config virtualenvs.in-project true
```

* If you want to uninstall poetry:
```console
$ python get-poetry.py --uninstall
```

---
## How to install, update and run

* Installing the project and all its development dependencies is quite simple:
```console
$ git clone https://gitlab.com/stephane.tzvetkov/sarar.git
$ cd sarar
$ poetry install
```

* Updating it is even more easy:
```console
$ git pull
```

* Running it just requires to execute the following:
```console
$ poetry run sarar
```

* E.g. if you want to run the
  [`check-deck`](https://sarar.readthedocs.io/en/latest/user-guide/commands/check-deck/) command:
```console
$ poetry run sarar check-deck -d /path/to/deck.deck
```

---
## Coding style

In order to save time and mental energy for more important matters,
[black](https://github.com/psf/black) is the tool used in this project to format Python code.

[black](https://github.com/psf/black) enforces formatting that conforms to [PEP
8](https://www.python.org/dev/peps/pep-0008/) which is a very sane style guide for Python (and a
very interesting reading).

Here are some resources in order to integrate `black` to your IDE:

- <https://github.com/psf/black/blob/main/docs/integrations/editors.md>

---
## Linting

The linters used for this project are [`flake8`](https://flake8.pycqa.org/en/latest/index.html) and
[`pylint`](https://pylint.pycqa.org/en/latest/), because they are well supported by
[`black`](https://black.readthedocs.io/en/stable/) (see [black compatible
configurations](https://black.readthedocs.io/en/stable/guides/using_black_with_other_tools.html)).

* [`flake8`](https://flake8.pycqa.org/en/latest/index.html) is a popular lint wrapper for python.
  Under the hood, it runs three other tools and combines their results:
    * [`pep8`](https://pep8.readthedocs.io/en/latest/) for checking style
    * [`pyflakes`](https://github.com/PyCQA/pyflakes) for checking syntax
    * [`mccabe`](https://github.com/pycqa/mccabe) for checking complexity

* [`pylint`](https://www.pylint.org/) is also a popular linter for python (now it even is the
  default python linter in VS Code). Under the hood, it runs those tools to combine their results:
    * [`astroid`](https://github.com/PyCQA/astroid) for a base representation of python source code
    * [`isort`](https://github.com/PyCQA/isort) for imports management
    * [`mccabe`](https://github.com/pycqa/mccabe) for checking complexity

Here are some resources in order to integrate `flake8` to your IDE:

- <https://nvuillam.github.io/mega-linter/descriptors/python_flake8/#ide-integration>

Here are some resources in order to integrate `pylint` to your IDE:

- <https://pylint.pycqa.org/en/latest/user_guide/ide-integration.html>
- <https://nvuillam.github.io/mega-linter/descriptors/python_pylint/#ide-integration>

---
## Debug

If you encounter tough errors during your developments, do not hesitate to use the [Python
debugger](https://docs.python.org/3/library/pdb.html).

If you want to check your `PYTHONPATH` you can use some of those commands:
```console
$ poetry run python -m site
$ poetry run bash -c "echo $PATH"
$ poetry run python -c "import sys, pprint; pprint.pprint(sys.path)"
```

---
## Tests

The test framework used for this project is [`pytest`](https://github.com/pytest-dev/pytest).

`pytest` has already been installed in your virtual environment when running `$ poetry install`,
you can learn more about it by reading its [official
documentation](https://docs.pytest.org/en/stable/index.html).

When adding, changing or removing some code to the SARAR project, make sure to update the tests
accordingly. And of course: **make sure the tests are successful**!

So, before pushing any new code, please check the results of the following tests:
```console
$ poetry run pytest # run tests
$ poetry run pytest -vv -r a # run tests with better looking output
$ poetry run pytest --black  # check black formatting
$ poetry run pytest --flake8 # check flake8 linter
$ poetry run pytest --pylint # check pylint linter
$ poetry run pytest --cov=sarar/ # check code coverage
$ poetry run pytest --pydocstyle # check doctrings
$ poetry run vulture ./sarar ./tests # check no dead code
```
You can also test all at once with this one-liner:
```console
$ poetry run pytest -vv -r a --black --flake8 --pylint --pydocstyle --cov=sarar/
$ poetry run vulture ./sarar ./tests # check no dead code
```

SARAR uses the [`click`](https://click.palletsprojects.com) module in order to create some command
line interfaces, here is how to create tests against `click`-based apps:
<https://click.palletsprojects.com/testing/>

Also, you can check the code coverage with the following command:
```console
$ poetry run pytest --cov=sarar --cov-report html
```

This will generate the `./htmlcov` folder. You can open it in your browser, e.g. with Firefox: `$
firefox ./htmlcov/index.html`, and check your coverage.

This will also generate a `.coverage` file used to create the coverage badge. For your information,
you can create/update the coverage badge with the following command:
```console
$ poetry run coverage-badge -o .coverage.svg
```

But you don't need to manually run the previous command, since it's already in the project's
[CI](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/.gitlab-ci.yml).

---
## Documentation

When adding, changing or removing some code to the SARAR project, make sure to update accordingly
the associated docstring and the documentation folder (`docs` at the root of the project). Feel
free to create, update or delete any file, and eventually reference it from the README.

### Markdown documentation

When documenting in the README or the
[docs](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs) folder, please respect the
[markdown syntax](https://www.markdownguide.org/getting-started/) in order to get nice and pleasant
files to read.

The [docs](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs) folder is also the root
directory for [MkDocs](https://www.mkdocs.org/) to work, [MkDocs](https://www.mkdocs.org/) is a
documentation oriented site generator: it will build a static HTML site from the markdown file and
will allow to format the documentation in a very nice way.

So, once you are done editing, please build the documentation and serve it, in order to check it:
```console
$ poetry run mkdocs serve -f .mkdocs.yaml
    > ...
    > Serving on http://127.0.0.1:8000
    > ...
```
Now just open `http://127.0.0.1:8000` in a web browser and make sure the rendering of your
modifications is OK.

In another console, in order to make sure your documentation does not contains any broken link, you
can run the following:
```console
$ poetry run linkchecker --check-extern --ignore-url=https://gitlab.com/stephane.tzvetkov/sarar/edit/ --ignore-url=https://fonts --ignore-url=https://sci-hub.st/ --no-robots http://127.0.0.1:8000
```

Note that the static HTML site build by [MkDocs](https://www.mkdocs.org/) is hosted on
[readthedocs](https://readthedocs.org/), so after pushing your markdown modifications
[readthedocs](https://readthedocs.org/) will serve them
[here](https://sarar.readthedocs.io/en/latest/).

### Docstring documentation

The source code of this project follows the [PEP 257 Docstring
conventions](https://www.python.org/dev/peps/pep-0257/), in order to get a standardize structure of
docstrings (i.e. what docstrings should contain, not what markup syntax should be used).

[PEP 257](https://www.python.org/dev/peps/pep-0257/) can be checked with:
```console
$ poetry run pytest -vv -r a --pydocstyle
```

The target markup syntax for formatting docstrings is the one recommended by the [PEP 287
`reStructuredText` Docstring Format](https://www.python.org/dev/peps/pep-0287/), i.e.
[`reStructuredText plaintext markup syntax`](https://docutils.sourceforge.io/rst.html). For a quick
overview: see [this example](https://stackoverflow.com/a/24385103), or [this
one](https://stackoverflow.com/a/40596167), or [this one](https://stackoverflow.com/a/5339352).

*Please, when adding or changing some source code, make sure to create or update associated
docstring(s).*

### Help documentation / Command documentation

The documentation of this project contains a little specificity: the Markdown docs in the [commands
folder](https://gitlab.com/stephane.tzvetkov/sarar/-/tree/master/docs/user-guide/commands) (e.g.
the [`check-deck`](../user-guide/commands/check-deck.md) doc) are parsed by SARAR when printing help
information in the command line (e.g. `$ sarar check-deck --help`).

This trick is possible thanks to the `doc_to_help(...)` function in
[`sarar.py`](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/sarar/sarar.py). So when
creating or modifying a command doc, please check the [Markdown rendering](#markdown-documentation)
**and** the associated `-h, --help` option output (e.g. `$ poetry run sarar check-deck --help`).

Do not hesitate to change the `doc_to_help(...)` function according to your needs.

!!! Info
    The target format for the help documentation and the command documentation is to be like a [man
    page](https://en.wikipedia.org/wiki/Man_page). Take inspiration from `$ man man` or `$ man
    git`.

---
### Dependencies

You can check all the dependency tree of SARAR by running `$ poetry show --tree`, a short
description for the top level dependencies will also be printed.

An other way to quickly check the top dependencies of SARAR is to read the [`./pyproject.toml`
file](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/pyproject.toml).

---
## License

See [how to use GNU licenses for your own software](https://www.gnu.org/licenses/gpl-howto.html).
In particular, don't forget to include the copyright notice at the top of each new source file you
would include.

---
## Release checklist

When a new version of SARAR is about to be released, make sure that:

- the [milestone](https://gitlab.com/stephane.tzvetkov/sarar/-/milestones) of the new release has
  been completed and closed (all associated issues must have been done and closed). The next one
  has been created.

- the dependencies have been updated
  ```console
  $ poetry update
  ```

- all the [tests](#tests) are passing (with 100% of tests coverage and no dead code)
  ```console
  $ poetry run pytest -v -r a --black --flake8 --pylint --cov=sarar/ -vv
  $ poetry run vulture ./sarar ./tests # check no dead code
  ```

- TODO: the size badge has been updated (TODO: run this into CI, not here manually)
  ```console
  $ SIZE=$(du -c --exclude=TODO --exclude=TODO | tail -1 | awk '{print $1}')
  $ curl https://img.shields.io/badge/repo%20size-$SIZE-informational -o .size.svg
  ```

- the [documentation](#documentation) has been updated and is looking nice
  ```console
  $ poetry run mkdocs serve -f .mkdocs.yaml
  $ poetry run linkchecker --check-extern --ignore-url=https://gitlab.com/stephane.tzvetkov/sarar/edit/ --ignore-url=https://fonts --ignore-url=https://sci-hub.st/ --no-robots http://127.0.0.1:8000
  ```

- the [change log](../CHANGELOG.md) has been updated

- the [project layout](./project-layout.md) has been updated

- the `__version__` variable in
  [`sarar.py`](https://gitlab.com/stephane.tzvetkov/sarar/-/blob/master/sarar/sarar.py) has been
  updated

- the locally build version is running without problems:
  ```console
  $ poetry build
  $ cd tests
  $ python -m venv .test_venv
  $ . ./.test_venv/bin/activate
  $ pip install --upgrade pip
  $ pip install ../dist/sarar-0.1.0.tar.gz
  $ sarar
  $ # now check your developments
  $ deactivate
  $ rm -r .test_venv
  ```

- the poetry version has been updated (see
  [here](https://python-poetry.org/docs/cli/#version) for more details, note that you can print the
  current version with `$ poetry version`):
    - in case of a major version bump: `$ poetry version major` (e.g. `1.3.0 -> 2.0.0`)
    - in case of a minor version bump: `$ poetry version minor` (e.g. `2.1.4 -> 2.2.0`)
    - in case of a patch version bump: `$ poetry version patch` (e.g. `4.1.1 -> 4.1.2`)

- the new poetry version has been published to `PyPi`: `$ poetry publish --build`

- the git tag version has been created, e.g. create a tag for the current commit to version
  `1.2.3`:
  ```console
  $ git tag 1.2.3 -m "bump version to 1.2.3"
  $ git push origin 1.2.3
  ```

- the readthedocs version associated to the previous git tag has been activated (see
  <https://readthedocs.org/projects/sarar/versions/>)

- the GitLab release has been created, e.g. to create a new release through the GitLab UI:
    - On the left sidebar, select Deployments -> Releases -> New release.
    - Select the associated tag name.
    - Set release title to the tag name.
    - Select the associated milestone.
    - Set the release notes to the associated [CHANGELOG](../CHANGELOG.md) section.
      ```console
      ##### Changelog

      ...
      ```

    - Add links
        - Add URL: `https://pypi.org/project/sarar/x.y.z/#files`, Link title: `SARAR x.y.z PyPi
          files`, Type: `Package`
        - Add URL: `https://pypi.org/project/sarar/x.y.z/`, Link title: `SARAR x.y.z PyPi`, Type:
          `Other`
        - Add URL: `https://sarar.readthedocs.io/en/x.y.z/`, Link title: `SARAR x.y.z
          documentation`, Type: `Documentation`
    - E.g. [release 0.1](https://gitlab.com/stephane.tzvetkov/sarar/-/releases/0.1)
